//
//  WebsocketError.swift
//  Farnsworth
//
//  Created by Marty on 05/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import Starscream

enum WebsocketError {
    case unauthorized
    case connectionFaiure
    case drop
    
    static func defineFromError(_ error: Error) -> WebsocketError {
        guard let error = error as? WSError else {
            return .drop
        }
        
        switch error.code {
        case 0:
            return .connectionFaiure
        case 401:
            return .unauthorized
        default:
            return .drop
        }
    }
}

//
//  RequestError.swift
//  Farnsworth
//
//  Created by Marty on 15/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum RequestError: Error {
    case notConnected
    case lostConnection
    case undefined
    
    static func defineFromError(_ error: Error) -> RequestError {
        guard let error = error as? URLError else {
            return .undefined
        }
        
        switch error.code {
        case .networkConnectionLost:
            return .lostConnection
        case .notConnectedToInternet:
            return .notConnected
        default:
            return .undefined
        }
    }
    
    func description() -> String {
        switch self {
        case .notConnected:
            return "Seems like you're offline"
        case .lostConnection:
            return "Lost connection"
        default:
            return "Request error"
        }
    }
}

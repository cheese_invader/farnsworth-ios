//
//  FError.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

/*
 Class 1
 
 [1.0] other error
 [1.1] access token is incorrect
 [1.2] 'first_name' is incorrect
 [1.3] 'last_name' is incorrect
 [1.4] 'birthday' is incorrect
 [1.5] 'gender' is incorrect
 [1.6] user has entered already
 [1.7] no such user
 */

enum FError: String, Encodable, Decodable {
    case err_1_0 = "1.0"
    case err_1_1 = "1.1"
    case err_1_2 = "1.2"
    case err_1_3 = "1.3"
    case err_1_4 = "1.4"
    case err_1_5 = "1.5"
    case err_1_6 = "1.6"
    case err_1_7 = "1.7"
    
    case err_2_0 = "2.0"
    case err_2_1 = "2.1"
    case err_2_2 = "2.2"
    
    var classDescription: String {
        switch self {
        case .err_1_0, .err_1_1, .err_1_2, .err_1_3, .err_1_4, .err_1_5, .err_1_6, .err_1_7:
            return "First login error"
        case .err_2_0, .err_2_1, .err_2_2:
            return "Chat error"
        }
    }
    
    var detailDescription: String {
        switch self {
        case .err_1_0: return "Sending information error"
        case .err_1_1: return "Access token is incorrect"
        case .err_1_2: return "First name is incorrect"
        case .err_1_3: return "Last name is incorrect"
        case .err_1_4: return "Birthday is incorrect"
        case .err_1_5: return "Gender is incorrect"
        case .err_1_6: return "We already got what we need"
        case .err_1_7: return "No such user"
            
        case .err_2_0: return "Creating chat error"
        case .err_2_1: return "Title is incorrect"
        case .err_2_2: return "Author is wrong"
        }
    }
}



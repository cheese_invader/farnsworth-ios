//
//  Person.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class Person {
    // Required
    let id     : String
    private(set) var version: Int
    private(set) var completeness = UserCompleteness.low
    
    
    // Can use if completeness is middle of high
    private(set) var firstName: String!
    private(set) var lastName : String!
    private(set) var gender   : UserGender!
    private(set) var status   : UserStatus!
    private(set) var lastSeen : Date!
    
    // Optional
    private(set) var username: String?
    private(set) var bio     : String?
    
    
    // Can use if completeness is high
    private(set) var photo: UIImage?
    
    
    
    class func makeFromFSCDEntity(_ entity: FSPerson) -> Person? {
        guard
            let id = entity.id,
            let rawCompleteness = entity.completeness,
            let completeness = UserCompleteness.init(rawValue: rawCompleteness)
        else {
            return nil
        }

        let person = Person(id: id, version: Int(entity.version))
        
        if completeness == .low {
            return person
        }
        
        guard
            let firstName = entity.firstName,
            let lastName  = entity.lastName,
            let rawGender = entity.gender,
            let gender    = UserGender.init(rawValue: rawGender),
            let rawStatus = entity.status,
            let status    = UserStatus.defineByStatusMessage(rawStatus),
            let lastSeen  = entity.lastSeen
        else {
            return nil
        }
        let username = entity.username
        let bio      = entity.bio
        
        person.setInfo(firstName: firstName, lastName: lastName, gender: gender, status: status, lastSeen: lastSeen, username: username, bio: bio)
        
        if completeness == .hihg {
            var img: UIImage?
            
            if let rawPhoto = entity.photo {
                img = UIImage(data: rawPhoto)
            }
            
            person.setPhoto(img)
        }
        
        return person
    }
    
    
    
    // USE ONLY THIS SCHEME  init() --> setInfo() --> Updates --> setPhoto() --> Updates
    
    init(id: String, version: Int) {
        self.id      = id
        self.version = version
    }
    
    func setInfo(firstName: String, lastName: String, gender: UserGender, status: UserStatus, lastSeen: Date, username: String?, bio: String?) {
        self.firstName    = firstName
        self.lastName     = lastName
        self.gender       = gender
        self.status       = status
        self.lastSeen     = lastSeen
        self.username     = username
        self.bio          = bio
        self.completeness = .middle
    }
    
    func setPhoto(_ photo: UIImage?) {
        guard completeness != .low else {
            return
        }
        self.photo = photo
        self.completeness = .hihg
    }
    
    
    // Updaters
    func update(firstName: String) {
        guard self.completeness != .low else {
            return
        }
        self.firstName = firstName
    }
    
    func update(lastName: String) {
        guard self.completeness != .low else {
            return
        }
        self.lastName = lastName
    }
    
    func update(gender: String) {
        guard self.completeness != .low else {
            return
        }
        self.gender = UserGender(rawValue: gender)
    }
    
    func update(username: String) {
        guard self.completeness != .low else {
            return
        }
        self.username = username
    }
    
    func update(bio: String) {
        self.bio = bio
    }
}

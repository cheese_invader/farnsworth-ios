//
//  Chat.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class Chat {
    // Required
    let id     : String
    private(set) var version: Int
    private(set) var completeness = ChatCompleteness.low
    
    // Can use if completeness is middle or high
    private(set) var title   : String!
    private(set) var created : Date!
    private(set) var author  : Person!
    private(set) var members  = [Person]()
    
    // Can use if completeness is high
    // Optional
    private(set) var photo: UIImage?
    
    
    
    class func makeFromFSCDEntity(_ entity: FSChat) -> Chat? {
        guard
            let id              = entity.id,
            let rawCompleteness = entity.completeness,
            let completeness    = ChatCompleteness.init(rawValue: rawCompleteness)
        else {
            return nil
        }
        
        let chat = Chat(id: id, version: Int(entity.version))
        
        if completeness == .low {
            return chat
        }
        
        guard
            let title = entity.title,
            let created = entity.created,
            let rawAuthor = entity.author,
            let nsSetMembers = entity.members,
            let rawMembers = Array(nsSetMembers) as? [FSPerson]
        else {
            return nil
        }
        
        guard let author = Person.makeFromFSCDEntity(rawAuthor) else {
            return nil
        }
        
        var members = [Person]()
        for rawMember in rawMembers {
            guard let member = Person.makeFromFSCDEntity(rawMember) else {
                return nil
            }
            members.append(member)
        }
        
        chat.setInfo(title: title, created: created, author: author, members: members)
        
        if completeness == .high {
            var img: UIImage?
            
            if let rawPhoto = entity.photo {
                img = UIImage(data: rawPhoto)
            }
            chat.setPhoto(img)
        }
        
        return chat
    }
    
    
    
    // USE ONLY THIS SCHEME init() --> setInfo() --> setPhoto()
    init(id: String, version: Int) {
        self.id      = id
        self.version = version
    }
    
    func setInfo(title: String, created: Date, author: Person, members: [Person]) {
        self.title   = title
        self.created = created
        self.author  = author
        self.members = members
        self.completeness = .middle
    }
    
    func setPhoto(_ photo: UIImage?) {
        self.photo = photo
        self.completeness = .high
    }
}

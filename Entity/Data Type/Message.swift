//
//  Message.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class Message {
    init(id: String, author: Person, chat: Chat) {
        self.id       = id
        self.author = author
        self.chat   = chat
    }
    
    
    // Required
    let id: String
    let author: Person
    let chat  : Chat
    
    
    // Optional
    var text: String?
    var imgs: [UIImage]?
    var data: Data?
    
    
    static func makeFromFSCDEntity(_ entity: FSMessage) -> Message? {
        guard
            let id = entity.id,
            let authorFS = entity.author,
            let author = Person.makeFromFSCDEntity(authorFS),
            let chatFS = entity.chat,
            let chat = Chat.makeFromFSCDEntity(chatFS)
            else {
                return nil
        }
        
        let message = Message(id: id, author: author, chat: chat)
        
        if let text = entity.text {
            message.text = text
        }
        if let data = entity.data {
            message.data = data
        }
        if
            let imgsData = entity.imgs,
            let images = ImageArrayCoder().decodeImageArray(imgsData) {
                message.imgs = images
        }
        
        return message
    }
}

//
//  Captain.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class Captain {
    init(me: Person, contacts: [Person], blackList: [Person]) {
        self.me = me
        self.contacts  = contacts
        self.blackList = blackList
    }
    
    var me: Person
    var contacts : [Person]
    var blackList: [Person]
    
    
    class func makeFromFSCDEntity(_ entity: FSCaptain) -> Captain? {
        guard
            let contacts  = entity.contacts,
            let blackList = entity.blackList,
            let meEntity  = entity.me,
            let me = Person.makeFromFSCDEntity(meEntity)
        else {
            return nil
        }
        var captainContacts = [Person]()
        var captainBlackList = [Person]()
        
        for user in contacts {
            guard
                let personEntity = (user as? FSPerson),
                let person = Person.makeFromFSCDEntity(personEntity)
            else {
                assertionFailure("Captain.makeFromFSCDEntity -- can't get contact from entity")
                continue
            }
            
            captainContacts.append(person)
        }
        for user in blackList {
            guard
                let personEntity = (user as? FSPerson),
                let person = Person.makeFromFSCDEntity(personEntity)
                else {
                    assertionFailure("Captain.makeFromFSCDEntity -- can't get black list user from entity")
                    continue
            }
            
            captainBlackList.append(person)
        }
        
        return Captain(me: me, contacts: captainContacts, blackList: captainBlackList)
    }
}

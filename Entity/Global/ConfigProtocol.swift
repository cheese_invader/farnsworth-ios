//
//  ConfigProtocol.swift
//  Farnsworth
//
//  Created by Marty on 14/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol ConfigProtocol {
    var isLogin: Bool          { get set }
    var userStatus: UserStatus? { get set }
    var device      : String?  { get set }
    var refreshToken: String?  { get set }
    var accessToken : String?  { get set }
    var userId      : String?  { get set }
    var serverURL: URL { get set }
}

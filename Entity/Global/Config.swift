//
//  Config.swift
//  Farnsworth
//
//  Created by Marty on 13/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

private let isLoginId = "isLoginId"
private let isUserLoginAtLeastOnceId = "isUserLoginAtLeastOnce"

private let refreshTokenId = "refreshToken"
private let  accessTokenId = "accessToken"
private let userIdId = "userId"
private let serverId = "server"
private let deviceId = "device"


class Config {
    static var shared = Config()
    
    // Singletone
    private init() {
        serverURL = Static.shared.baseServerURL
    }
}


extension Config: ConfigProtocol {
    var isLogin: Bool {
        get {
            return UserDefaults.standard.bool(forKey: isLoginId)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: isLoginId)
        }
    }
    
    var userStatus: UserStatus? {
        get {
            guard let userStatusMessage = UserDefaults.standard.string(forKey: isUserLoginAtLeastOnceId) else {
                return nil
            }
            return UserStatus.defineByStatusMessage(userStatusMessage)
        }
        set {
            guard let value = newValue else {
                return
            }
            UserDefaults.standard.set(value.rawValue, forKey: isUserLoginAtLeastOnceId)
        }
    }
    
    var device: String? {
        get {
            return UserDefaults.standard.string(forKey: deviceId)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: deviceId)
        }
    }
    
    var refreshToken: String? {
        get {
            return UserDefaults.standard.string(forKey: refreshTokenId)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: refreshTokenId)
        }
    }
    
    var accessToken: String? {
        get {
            return UserDefaults.standard.string(forKey: accessTokenId)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: accessTokenId)
        }
    }
    
    var userId: String? {
        get {
            return UserDefaults.standard.string(forKey: userIdId)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: userIdId)
        }
    }
    
    var serverURL: URL {
        get {
            guard let server = UserDefaults.standard.string(forKey: serverId) else {
                return Static.shared.baseServerURL
            }
            return URL(string: server) ?? Static.shared.baseServerURL
        }
        set {
            UserDefaults.standard.set(newValue.absoluteString, forKey: serverId)
        }
    }
}

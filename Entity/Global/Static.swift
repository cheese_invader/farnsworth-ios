//
//  Static.swift
//  Farnsworth
//
//  Created by Marty on 14/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

private let globalip = "91.144.161.172"
private let localip = "192.168.31.211"
private let customip = "172.20.10.2"

private let websocketPort = ":3049"
private let serverPort    = ":2727"

class Static {
    static var shared = Static()
    
    // Singletone
    private init() {
    }
    
    let baseServerURL   = URL(string: "http://" + localip + serverPort)!
    let baseWebsocketURL = URL(string: "ws://"  + localip + websocketPort)!
    let baseDevice = "iOS"
    let apiLogin = "/authorization/login/"
    let apiConfirm = "/authorization/confirm/"
    let apiFirstLogin = "/firstEnter/set/"
    let minutesToConfirmDeadline = 3
}

//
//  UserGender.swift
//  Farnsworth
//
//  Created by Marty on 29/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation


// Server doesn't use camelCase. DO NOT FORGET TO ADD A LITERAL IN RAW VALUE
private let   maleMessage = "male"
private let femaleMessage = "female"
private let  otherMessage = "other"


enum UserGender: String, Encodable, Decodable {
    case male   = "male"
    case female = "female"
    case other  = "other"
}

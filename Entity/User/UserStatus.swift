//
//  UserStatus.swift
//  Farnsworth
//
//  Created by Marty on 29/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation


private let neverLoginMessage = "never_login"
private let onlineMessage     = "online"
private let offlineMessage    = "offline"
private let deletedMessage    = "deleted"


enum UserStatus: String, Encodable, Decodable {
    case neverLogin = "never_login"
    case online     = "online"
    case offline    = "offline"
    case deleted    = "deleted"
    
    static func defineByStatusMessage(_ message: String) -> UserStatus? {
        switch message {
        case neverLoginMessage:
            return .neverLogin
        case onlineMessage:
            return .online
        case offlineMessage:
            return .offline
        case deletedMessage:
            return .deleted
        default:
            return nil
        }
    }
}



extension UserStatus: Equatable {
    static func == (lhs: UserStatus, rhs: UserStatus) -> Bool {
        switch (lhs, rhs) {
        case (.neverLogin, .neverLogin):
            return true
        case (.online, .online):
            return true
        case (.offline, .offline):
            return true
        case (.deleted, .deleted):
            return true
        default:
            return false
        }
    }
}

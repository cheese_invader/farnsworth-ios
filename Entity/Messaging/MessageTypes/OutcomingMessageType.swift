//
//  OutcomingMessageType.swift
//  Farnsworth
//
//  Created by Marty on 08/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum OutcomingMessageType: String, Encodable, Decodable {
    case createChat
    case needChatsInfo
    case findUsers
    case updateMyInfo
    case getUsersPhotos
    case getUsersInfo
}

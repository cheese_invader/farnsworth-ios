//
//  IncomingMessageType.swift
//  Farnsworth
//
//  Created by Marty on 08/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum IncomingMessageType: String, Encodable, Decodable {
    case createChatError
    case newChatIsComing
    case yourPrivateData
    case chatsDetailInfo
    case foundUsers
    case usersPhoto
    case usersInfo
}

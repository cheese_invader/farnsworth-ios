//
//  CreateChatErrorPayload.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct CreateChatErrorPayload: Encodable, Decodable {
    let ferror: FError
}

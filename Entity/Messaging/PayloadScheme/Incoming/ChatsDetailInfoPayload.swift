//
//  ChatsDetailInfoPayload.swift
//  Farnsworth
//
//  Created by Marty on 14/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct ChatsDetailInfoPayload: Encodable, Decodable {
    let chats: [NewChatIsComingPayload]
}

//
//  YourPrivateDataPayload.swift
//  Farnsworth
//
//  Created by Marty on 08/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct YourProvateDataPayload: Encodable, Decodable {
    let id: String
    let version: Int
    let status: StatusPayload
    let firstName: String
    let lastName: String
    let gender: UserGender
    
    let chatList : [CompressedEntityPayload]
    let contacts : [CompressedEntityPayload]
    let blackList: [CompressedEntityPayload]
    
    let username: String?
    let bio: String?
}

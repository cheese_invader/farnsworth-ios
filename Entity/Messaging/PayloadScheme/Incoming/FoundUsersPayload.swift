//
//  FoundUsersPayload.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct FoundUsersPayload: Encodable, Decodable {
    let users: [SearchedUserPayload]
}

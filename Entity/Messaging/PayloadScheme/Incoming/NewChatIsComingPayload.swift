//
//  NewChatIsComingPayload.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct NewChatIsComingPayload: Encodable, Decodable {
    let id: String
    let version: Int
    let title: String
    let created: Date
    let author: String
    let members: [String]
}

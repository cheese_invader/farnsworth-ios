//
//  SearchedUserPayload.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct SearchedUserPayload: Encodable, Decodable {
    let id: String
    let firstName: String
    let lastName: String
    let status: StatusPayload
    let username: String?
}

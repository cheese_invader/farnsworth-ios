//
//  StatusPayload.swift
//  Farnsworth
//
//  Created by Marty on 08/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct StatusPayload: Encodable, Decodable {
    let status: UserStatus
    let lastSeen: Date
}

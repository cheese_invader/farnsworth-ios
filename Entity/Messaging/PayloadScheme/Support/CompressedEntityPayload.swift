//
//  CompressedEntityPayload.swift
//  Farnsworth
//
//  Created by Marty on 09/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct CompressedEntityPayload: Encodable, Decodable {
    let i: String
    let v: Int
}

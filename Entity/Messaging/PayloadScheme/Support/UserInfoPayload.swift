//
//  UserInfoPayload.swift
//  Farnsworth
//
//  Created by Marty on 31/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct UserInfoPayload: Encodable, Decodable {
    let id: String
    let version: Int
    let status: StatusPayload
    
    let firstName: String
    let lastName: String
    let gender: UserGender
    
    let username: String?
    let bio: String?
}

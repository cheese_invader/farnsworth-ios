//
//  GetUserPhotosPayload.swift
//  Farnsworth
//
//  Created by Marty on 30/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct GetUserPhotosPayload: Encodable, Decodable {
    let ids: [String]
}

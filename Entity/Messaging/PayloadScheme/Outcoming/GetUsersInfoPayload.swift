//
//  GetUsersInfoPayload.swift
//  Farnsworth
//
//  Created by Marty on 31/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct GetUsersInfoPayload: Encodable, Decodable {
    let ids: [String]
}

//
//  UpdateMyInfoPayload.swift
//  Farnsworth
//
//  Created by Marty on 22/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct UpdateMyInfoPayload: Encodable, Decodable {
    let firstName: String?
    let lastName : String?
    let gender   : String?
    let username : String?
    let bio      : String?
    let photo    : Data?
}

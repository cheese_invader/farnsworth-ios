//
//  FSIncomingMessageSchema.swift
//  Farnsworth
//
//  Created by Marty on 08/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct FSIncomingMessageSchema: Encodable, Decodable {
    let messageType: IncomingMessageType
    let payload: String
}

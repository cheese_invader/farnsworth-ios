//
//  FSOutcomingMessageSchema.swift
//  Farnsworth
//
//  Created by Marty on 08/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct FSOutcomingMessageSchema: Encodable, Decodable {
    let messageType: OutcomingMessageType
    let payload: String
}

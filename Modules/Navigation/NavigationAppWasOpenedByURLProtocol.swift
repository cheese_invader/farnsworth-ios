//
//  NavigationAppWasOpenedByURLProtocol.swift
//  Farnsworth
//
//  Created by Marty on 15/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol NavigationAppWasOpenedByURLProtocol {
    func appWasOpenedByURL(url: URL)
    func openingAppByURLFailure()
}

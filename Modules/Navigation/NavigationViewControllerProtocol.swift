//
//  NavigationViewControllerProtocol.swift
//  Farnsworth
//
//  Created by Marty on 24/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol NavigationViewControllerProtocol {
    func identifyTheDevice()
    func showAlert(title: String, message: String)
    func confirmationSucceed()
    func confirmationFailed(_ error: ConfirmError)
    func firstEnterSucceed()
}

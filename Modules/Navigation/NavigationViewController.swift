//
//  NavigationViewController.swift
//  Farnsworth
//
//  Created by Marty on 11/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {
    // MARK: - Modules -
    private var confirm: ConfirmInteractorProtocol?
    
    
    private static var navigationController: NavigationViewController?
    
    
    // Only for appDelegate!
    static var shared: NavigationViewController? {
        return NavigationViewController.navigationController
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        NavigationViewController.navigationController = self
        identifyTheDevice()
        
        setNavigationBarHidden(true, animated: false)
        guard let userStatus = Config.shared.userStatus else {
            presentLoginModule(animated: false)
            return
        }
        
        switch userStatus {
        case .neverLogin:
            presentFirstEnterModule(animated: false)
        default:
            presentChatListModule(animated: false)
        }
    }
}


extension NavigationViewController: NavigationAppWasOpenedByURLProtocol {
    func appWasOpenedByURL(url: URL) {
        popToRootViewController(animated: true)
        
        guard let message = url.host?.removingPercentEncoding else {
            openingAppByURLFailure()
            return
        }
        
        switch message {
        case "confirm":
            presentConfirmModule(url: url)
        default:
            openingAppByURLFailure()
        }
    }
    
    func openingAppByURLFailure() {
        let alert = UIAlertController(title: "Unsupported link", message: "Something was wrong", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Got it", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
}


extension NavigationViewController: NavigationViewControllerProtocol {
    func identifyTheDevice() {
        Config.shared.device = UIDevice.current.userInterfaceIdiom == .pad ? "iPad" : "iPhone"
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Got it", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }

    func confirmationSucceed() {
        if Config.shared.userStatus == .neverLogin {
            presentFirstEnterModule(animated: true)
        } else {
            presentChatListModule(animated: true)
        }
    }
    
    func confirmationFailed(_ error: ConfirmError) {
        showAlert(title: "Confirmation failed", message: error.description() + ". Try again later")
    }
    
    func firstEnterSucceed() {
        presentChatListModule(animated: true)
    }
}


extension NavigationViewController: NavigationSwitcherProtocol {
    func presentLoginModule(animated: Bool) {
        setViewControllers([LoginRouter.assemble(embadedIn: self)], animated: animated)
    }
    
    func presentConfirmModule(url: URL) {
        guard let loginToken = url["loginToken"], let userId = url["userId"] else {
            openingAppByURLFailure()
            return
        }
        confirm = ConfirmRouter.assemble(embadedIn: self)
        confirm?.confirm(loginToken: loginToken, userId: userId)
    }
    
    func presentFirstEnterModule(animated: Bool) {
        setViewControllers([FirstEnterRouter.assemble(embadedIn: self)], animated: animated)
    }
    
    func presentChatListModule(animated: Bool) {
        setViewControllers([ChatListRouter.assemble(embadedIn: self)], animated: animated)
        setNavigationBarHidden(false, animated: animated)
    }
}

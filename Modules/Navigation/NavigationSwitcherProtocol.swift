//
//  NavigationSwitcherProtocol.swift
//  Farnsworth
//
//  Created by Marty on 29/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol NavigationSwitcherProtocol {
    func presentLoginModule(animated: Bool)
    func presentConfirmModule(url: URL)
    func presentFirstEnterModule(animated: Bool)
    func presentChatListModule(animated: Bool)
}

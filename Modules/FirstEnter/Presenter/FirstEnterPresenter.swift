//
//  FirstEnterPresenter.swift
//  Farnsworth
//
//  Created by Marty on 25/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation


class FirstEnterPresenter {
    // MARK: - VIPER -
    private weak var view : FirstEnterViewControllerProtocol?
    private var interactor: FirstEnterInteractorProtocol?
    private var router    : FirstEnterRouterProtocol?
    
    private var currentTextField = FirstEnterField.firstName
}


extension FirstEnterPresenter: FirstEnterPresenterProtocol {
    func setup(view: FirstEnterViewControllerProtocol, router: FirstEnterRouterProtocol, interactor: FirstEnterInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    func userEnteredFirstName(_ firstName: String?) {
        guard let firstName = firstName, !firstName.isEmpty else {
            view?.shakeField(.firstName)
            view?.setFirstNameTitle(interactor?.getFirstName() ?? "")
            return
        }
        interactor?.setFirstName(firstName)
    }
    
    func userEnteredLastName(_ lastName: String?) {
        guard let lastName = lastName, !lastName.isEmpty else {
            view?.shakeField(.lastName)
            view?.setLastNameTitle(interactor?.getLastName() ?? "")
            return
        }
        interactor?.setLastName(lastName)
    }
    
    func userEnteredBirthday(_ birthday: Date?) {
        guard let birthday = birthday else {
            view?.shakeField(.birthday)
            view?.setBirthday(interactor?.getBirthday())
            return
        }
        view?.isSendButtonEnabled = true
        interactor?.setBirthday(birthday)
    }
    
    func userWantToSendForm() {
        if let emptyForm = interactor?.firstEmptyField() {
            view?.shakeField(emptyForm)
            view?.makeFirstResponder(emptyForm)
            return
        }
        view?.isSendButtonEnabled = false
        view?.setMessageTitle("")
        view?.isActivityIndicatorAnimating = true
        interactor?.sendUserInformation()
    }
    
    func switchToTextField(_ textField: FirstEnterField) {
        view?.endEditingField(currentTextField, duration: 1.5, delay: 0)
        view?.showField(textField, duration: 1.5, delay: 1)
        view?.makeFirstResponder(textField)
        currentTextField = textField
    }
    
    @discardableResult
    func incorrectValueForField(_ textField: FirstEnterField) -> Bool {
        return view?.makeFirstResponder(textField) ?? false
    }
    
    func removeFirstResponder() {
        view?.removeFirstResponder()
    }
    
    func completeEntering() {
        view?.removeFirstResponder()
        view?.endEditingField(currentTextField, duration: 1.5, delay: 0)
    }
    
    func sendingSuccess() {
        view?.isActivityIndicatorAnimating = false
        view?.setMessageTitle("Success")
        router?.switchNextModule()
    }
    
    func sendingFailure(_ error: FirstEnterError) {
        view?.isActivityIndicatorAnimating = false
        view?.setMessageTitle(error.description())
        view?.isSendButtonEnabled = true
        
        switch error {
        case .err_1_7, .err_1_6, .err_1_1:
            router?.swotchToLoginModule()
        case .err_1_2: view?.makeFirstResponder(.firstName)
        case .err_1_3: view?.makeFirstResponder(.lastName)
        case .err_1_4: view?.makeFirstResponder(.birthday)
        default:
            break
        }
    }
}

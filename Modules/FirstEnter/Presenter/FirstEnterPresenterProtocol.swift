//
//  FirstEnterPresenterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 25/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol FirstEnterPresenterProtocol: class {
    func setup(view: FirstEnterViewControllerProtocol, router: FirstEnterRouterProtocol, interactor: FirstEnterInteractorProtocol)
    
    // View ->
    func userEnteredFirstName(_ firstName: String?)
    func userEnteredLastName(_ lastName: String?)
    func userEnteredBirthday(_ birthday: Date?)
    func userWantToSendForm()
    
    // Interactor ->
    func switchToTextField(_ textField: FirstEnterField)
    @discardableResult
    func incorrectValueForField(_ textField: FirstEnterField) -> Bool
    func removeFirstResponder()
    func completeEntering()
    func sendingSuccess()
    func sendingFailure(_ error: FirstEnterError)
}

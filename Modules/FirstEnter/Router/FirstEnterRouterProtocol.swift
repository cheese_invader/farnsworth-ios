//
//  FirstEnterRouterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 25/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol FirstEnterRouterProtocol: class {
    static func assemble(embadedIn navigationController: NavigationViewController) -> FirstEnterViewController
    
    func switchNextModule()
    func swotchToLoginModule()
}

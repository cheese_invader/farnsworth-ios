//
//  FirstEnterRouter.swift
//  Farnsworth
//
//  Created by Marty on 25/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "FirstEnter"
private let storyboardID   = "FirstEnterStoryboard"


class FirstEnterRouter {
    static let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
    
    private weak var navigation: NavigationViewController?
    
    
    // VIPER
    private weak var presenter: FirstEnterPresenterProtocol?
    
    
    init(presenter: FirstEnterPresenterProtocol, navigationController: NavigationViewController) {
        self.presenter  = presenter
        self.navigation = navigationController
    }
}


extension FirstEnterRouter: FirstEnterRouterProtocol {
    static func assemble(embadedIn navigationController: NavigationViewController) -> FirstEnterViewController {
        let firstLoginvc = FirstEnterRouter.storyboard.instantiateViewController(withIdentifier: storyboardID) as! FirstEnterViewController
        
        let presenter  = FirstEnterPresenter()
        let interactor = FirstEnterInteractor(presenter: presenter)
        let router     = FirstEnterRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: firstLoginvc, router: router, interactor: interactor)
        firstLoginvc  .setup(presenter: presenter)
        
        return firstLoginvc
    }
    
    func switchNextModule() {
        navigation?.firstEnterSucceed()
    }
    
    func swotchToLoginModule() {
        navigation?.presentLoginModule(animated: true)
    }
}

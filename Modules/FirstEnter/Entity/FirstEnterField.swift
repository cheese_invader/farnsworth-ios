//
//  FirstEnterTextFields.swift
//  Farnsworth
//
//  Created by Marty on 26/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum FirstEnterField {
    case firstName
    case lastName
    case birthday
}

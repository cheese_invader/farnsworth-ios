//
//  FirstEnterError.swift
//  Farnsworth
//
//  Created by Marty on 29/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

/*
 Class 1
 
 [1.0] other error
 [1.1] access token is incorrect
 [1.2] 'first_name' is incorrect
 [1.3] 'last_name' is incorrect
 [1.4] 'birthday' is incorrect
 [1.5] 'gender' is incorrect
 [1.6] user has entered already
 [1.7] no such user
 */

enum FirstEnterError {
    static func defineFromRequestError(_ requestError: RequestError) -> FirstEnterError {
        return .requestError(requestError)
    }
    
    static func defineByMessage(_ message: String) -> FirstEnterError? {
        switch message {
        case "1.0":
            return .err_1_0
        case "1.1":
            return .err_1_1
        case "1.2":
            return .err_1_2
        case "1.3":
            return .err_1_3
        case "1.4":
            return .err_1_4
        case "1.5":
            return .err_1_5
        case "1.6":
            return .err_1_6
        case "1.7":
            return .err_1_7
        default:
            return nil
        }
    }
    
    case err_1_0
    case err_1_1
    case err_1_2
    case err_1_3
    case err_1_4
    case err_1_5
    case err_1_6
    case err_1_7
    
    case requestError(RequestError)
    
    func description() -> String {
        switch self {
        case .err_1_0: return "Sending information error"
        case .err_1_1: return "Access token is incorrect"
        case .err_1_2: return "First name is incorrect"
        case .err_1_3: return "Last name is incorrect"
        case .err_1_4: return "Birthday is incorrect"
        case .err_1_5: return "Gender is incorrect"
        case .err_1_6: return "We already got what we need"
        case .err_1_7: return "No such user"
        case .requestError(let requestError): return requestError.description()
        }
    }
}


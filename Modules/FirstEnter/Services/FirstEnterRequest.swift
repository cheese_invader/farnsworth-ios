//
//  FirstEnterRequest.swift
//  Farnsworth
//
//  Created by Marty on 29/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation


private let dateFormat = "yyyy-MM-dd"


class FirstEnterRequest {
    private var task: URLSessionDataTask?
    
    var url = Config.shared.serverURL.appendingPathComponent(Static.shared.apiFirstLogin)
    var completionSuccess: (() -> Void)?
    var completionFailure: ((FirstEnterError) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeTaskWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                let requestError = RequestError.defineFromError(error)
                let firstEnterError = FirstEnterError.defineFromRequestError(requestError)
                self.completionFailure?(firstEnterError)
                return
            }
            
            guard let urlResponse = response as? HTTPURLResponse, let data = data else {
                self.completionFailure?(.err_1_0)
                return
            }
            
            switch urlResponse.statusCode {
            case 200:
                Config.shared.userStatus = .offline
                self.completionSuccess?()
                return
            case 401:
                self.completionFailure?(.err_1_1)
                return
            case 400:
                guard let dataSerialized = try? JSONSerialization.jsonObject(with: data, options: []), let params = dataSerialized as? [String: String] else {
                    break
                }
                if let ferrorMessage = params["ferror"] {
                    let ferror = FirstEnterError.defineByMessage(ferrorMessage) ?? .err_1_0
                    self.completionFailure?(ferror)
                    return
                }
            default:
                break
            }
            self.completionFailure?(.err_1_0)
        })
    }
}


// request
extension FirstEnterRequest {
    func perform(firstName: String, lastName: String, birthday: Date, gender: UserGender, completionSuccess: (() -> Void)?, completionFailure: ((FirstEnterError) -> Void)?) {
        task?.cancel()
        
        self.completionSuccess = completionSuccess
        self.completionFailure = completionFailure
        
        perform(firstName: firstName, lastName: lastName, birthday: birthday, gender: gender)
    }
    
    func perform(firstName: String, lastName: String, birthday: Date, gender: UserGender) {
        task?.cancel()
        
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        let birthdayString = formatter.string(from: birthday)
        
        guard let accessToken = Config.shared.accessToken else {
            completionFailure?(.err_1_1)
            return
        }
        
        let params = ["first_name": firstName, "last_name": lastName, "birthday": birthdayString, "gender": gender.rawValue]
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
        } catch {
            completionFailure?(.err_1_0)
            return
        }
        
        composeTaskWithRequest(request)
        
        task?.resume()
    }
}

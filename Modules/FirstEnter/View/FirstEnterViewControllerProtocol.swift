//
//  FirstEnterViewControllerProtocol.swift
//  Farnsworth
//
//  Created by Marty on 25/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

protocol FirstEnterViewControllerProtocol: class {
    func setup(presenter: FirstEnterPresenterProtocol)
    
    
    // Send button
    var isSendButtonEnabled: Bool { get set }
    
    // Activity indicator
    var isActivityIndicatorAnimating: Bool { get set }
    
    
    // Text fields stuff
    func       showField(_ textField: FirstEnterField, duration: Double, delay: Double)
    func endEditingField(_ textField: FirstEnterField, duration: Double, delay: Double)
    func shakeField(_ textField: FirstEnterField)
    
    // Text fields set values
    func setFirstNameTitle(_ title: String)
    func setLastNameTitle(_ title: String)
    func setBirthday(_ birthday: Date?)
        
    // First responder
    // If textField is hidden -> false
    @discardableResult
    func makeFirstResponder(_ textField: FirstEnterField) -> Bool
    func removeFirstResponder()
    
    // Message title
    func setMessageTitle(_ message: String)
}

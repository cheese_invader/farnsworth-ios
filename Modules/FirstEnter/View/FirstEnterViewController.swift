//
//  FirstEnterViewController.swift
//  Farnsworth
//
//  Created by Marty on 25/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit


private let appearDuration = 0.4

private let lineOffsetSide: CGFloat   = 50
private let lineOffsetBottom: CGFloat = 15

private let appearingPosition: CGFloat = 100
private let firstNameEndPosition: CGFloat = -100
private let  lastNameEndPosition: CGFloat = -60
private let  birthdayEndPosition: CGFloat = -20

private let dateFormat = "MMM d, yyyy"
private let minDate = "Jan 1, 1900"


class FirstEnterViewController: UIViewController {    
    // MARK: - VIPER -
    private var presenter: FirstEnterPresenterProtocol?
    
    private var datePicker: UIDatePicker?
    
    // MARK: - Constraints -
    
    @IBOutlet weak var blurVisualEffectView: UIVisualEffectView!
    private var blurEffect: UIVisualEffect?
    
    @IBOutlet weak var firstNameTextFieldCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var  lastNameTextFieldCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var  birthdayTextFieldCenterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var firstNameTextFieldHorizontallyConstraint: NSLayoutConstraint!
    @IBOutlet weak var lastNameTextFieldHorizontallyConstraint : NSLayoutConstraint!
    @IBOutlet weak var birthdayTextFieldHorizontallyConstraint : NSLayoutConstraint!
    
    
    // MARK: - Outlets -
    @IBOutlet weak var firstNameTextField: ShakeTextField!
    @IBOutlet weak var lastNameTextField: ShakeTextField!
    @IBOutlet weak var birthdayTextField: ShakeTextField!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    
    @IBOutlet weak var sendButton: RoundButton!
    
    @IBOutlet weak var messageTitle: UILabel!
    
    @IBOutlet weak var firstEnterActivityIndicator: UIActivityIndicatorView!
    
    // MARK: - Actions -
    @IBAction func sendButtonWasTapped(_ sender: UIButton) {
        presenter?.userWantToSendForm()
    }
    
    @IBAction func firstNameTextFieldPrimaryActionTriggered(_ sender: UITextField) {
        presenter?.userEnteredFirstName(firstNameTextField.text)
    }
    
    @IBAction func lastNameTextFieldPrimaryActionTriggered(_ sender: UITextField) {
        presenter?.userEnteredLastName(lastNameTextField.text)
    }
    
    @objc private func birthdayDatePickerDoneButtonWasTapped() {
        guard let datePicker = datePicker else {
            return
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        birthdayTextField.text = formatter.string(from: datePicker.date)
        
        setBirthday(datePicker.date)
        presenter?.userEnteredBirthday(datePicker.date)
    }
    
    
    
    // MARK: - VC stuff -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareForAppearing()
        
        setupDatePicker()
        
        firstNameTextFieldCenterConstraint.constant = appearingPosition
        lastNameTextFieldCenterConstraint .constant = appearingPosition
        birthdayTextFieldCenterConstraint .constant = appearingPosition
        
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        drawLineInCenter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        showField(.firstName, duration: 1.5, delay: 0)
        UIView.animate(withDuration: appearDuration) { [weak self] in
            guard let self = self else {
                return
            }
            
            self.blurVisualEffectView.effect = self.blurEffect
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        blurVisualEffectView.effect = nil
    }
}


// MARK: - Helpers -

extension FirstEnterViewController {
    private func prepareForAppearing() {
        blurEffect = blurVisualEffectView.effect
        blurVisualEffectView.effect = nil
    }
    
    private func drawLineInCenter() {
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: lineOffsetSide, y: view.center.y + lineOffsetBottom))
        path.addLine(to: CGPoint(x: view.bounds.width - lineOffsetSide, y: view.center.y + lineOffsetBottom))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = #colorLiteral(red: 1, green: 0.7882352941, blue: 0, alpha: 1)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 2
        
        view.layer.addSublayer(shapeLayer)
    }
    
    // Moving text fields
    private func showTextField(_ textFieldToMove: UITextField, duration: Double, delay: Double) {
        textFieldToMove.alpha = 0
        textFieldToMove.isHidden = false
        textFieldToMove.isEnabled = true
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            textFieldToMove.alpha = 1
            
            strongSelf.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    private func showLabel(_ label: UILabel, duration: Double, delay: Double) {
        label.alpha = 0
        label.isHidden = false
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            label.alpha = 1
            
            strongSelf.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    private func endEditingTextField(_ textFieldToMove: UITextField, duration: Double, delay: Double) {
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    private func setupDatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        
        // Pitcher
        datePicker = UIDatePicker()
        datePicker?.maximumDate = Date()
        datePicker?.minimumDate = formatter.date(from: minDate)
        datePicker?.datePickerMode = .date
        datePicker?.setValue(UIColor.white, forKeyPath: "textColor")
        datePicker?.backgroundColor = UIColor.black
        
        birthdayTextField.inputView = datePicker

        // Toolbar
        let toolbar = UIToolbar()
        toolbar.barTintColor = UIColor.black
        toolbar.sizeToFit()
        
        // Done button
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(birthdayDatePickerDoneButtonWasTapped))
        doneButton.tintColor = #colorLiteral(red: 1, green: 0.7882352941, blue: 0, alpha: 1)
        toolbar.setItems([doneButton], animated: true)
        
        birthdayTextField.inputAccessoryView = toolbar
    }
}


extension FirstEnterViewController: FirstEnterViewControllerProtocol {
    func setup(presenter: FirstEnterPresenterProtocol) {
        self.presenter = presenter
    }
    
    
    var isActivityIndicatorAnimating: Bool {
        get {
            return firstEnterActivityIndicator.isAnimating
        }
        set {
            if newValue {
                firstEnterActivityIndicator.startAnimating()
            } else {
                firstEnterActivityIndicator.stopAnimating()
            }
        }
    }
    
    var isSendButtonEnabled: Bool {
        get {
            return sendButton.isEnabled
        }
        set {
            sendButton.isEnabled = newValue
        }
    }
    
    
    func showField(_ textField: FirstEnterField, duration: Double, delay: Double) {
        var textFieldToMove: UITextField!
        
        switch textField {
        case .firstName:
            firstNameTextFieldCenterConstraint.constant = 0
            textFieldToMove = firstNameTextField
        case .lastName:
            lastNameTextFieldCenterConstraint.constant = 0
            textFieldToMove = lastNameTextField
        case .birthday:
            birthdayTextFieldCenterConstraint.constant = 0
            textFieldToMove = birthdayTextField
        }
        
        showTextField(textFieldToMove, duration: duration, delay: delay)
    }
    
    func endEditingField(_ textField: FirstEnterField, duration: Double, delay: Double) {
        var textFieldEndEditing: UITextField!
        var labelToShow: UILabel!
        
        switch textField {
        case .firstName:
            firstNameTextFieldCenterConstraint.constant = firstNameEndPosition
            firstNameTextFieldHorizontallyConstraint.isActive = false
            textFieldEndEditing = firstNameTextField
            labelToShow         = firstNameLabel
        case .lastName:
            lastNameTextFieldCenterConstraint.constant = lastNameEndPosition
            lastNameTextFieldHorizontallyConstraint.isActive = false
            textFieldEndEditing = lastNameTextField
            labelToShow         = lastNameLabel
        case .birthday:
            birthdayTextFieldCenterConstraint.constant = birthdayEndPosition
            birthdayTextFieldHorizontallyConstraint.isActive = false
            textFieldEndEditing = birthdayTextField
            labelToShow         = birthdayLabel
        }
        
        endEditingTextField(textFieldEndEditing, duration: duration, delay: delay)
        showLabel(labelToShow, duration: duration, delay: delay)
    }
    
    func shakeField(_ textField: FirstEnterField) {
        switch textField {
        case .firstName:
            firstNameTextField.shake()
        case .lastName:
            lastNameTextField.shake()
        case .birthday:
            birthdayTextField.shake()
        }
    }
    
    func setFirstNameTitle(_ title: String) {
        firstNameTextField.text = title
    }
    
    func setLastNameTitle(_ title: String) {
        lastNameTextField.text = title
    }
    
    func setBirthday(_ birthday: Date?) {
        // TODO: implement date pitcher
    }
    
    
    @discardableResult
    func makeFirstResponder(_ textField: FirstEnterField) -> Bool {
        switch textField {
        case .firstName:
            return firstNameTextField.becomeFirstResponder()
        case .lastName:
            return lastNameTextField.becomeFirstResponder()
        case .birthday:
            return birthdayTextField.becomeFirstResponder()
        }
    }
    
    func removeFirstResponder() {
        firstNameTextField.resignFirstResponder()
        lastNameTextField.resignFirstResponder()
        birthdayTextField.resignFirstResponder()
    }
    
    func setMessageTitle(_ message: String) {
        messageTitle.text = message
    }
}


//
//  FirstEnterInteractor.swift
//  Farnsworth
//
//  Created by Marty on 25/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

class FirstEnterInteractor {
    // VIPER
    private weak var presenter: FirstEnterPresenterProtocol?
    
    private var firstEnterRequest = FirstEnterRequest()
    
    private var firstName: String?
    private var lastName : String?
    private var birthday: Date?
    
    init(presenter: FirstEnterPresenterProtocol) {
        self.presenter = presenter
        
        firstEnterRequest.completionSuccess = { [weak self] in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.sendingSuccess()
            }
        }
        
        firstEnterRequest.completionFailure = { [weak self] (sendingError) in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.sendingFailure(sendingError)
            }
        }
    }
}


extension FirstEnterInteractor: FirstEnterInteractorProtocol {
    func setFirstName(_ firstName: String) {
        if self.firstName != nil {
            if !(presenter?.incorrectValueForField(.lastName) ?? false) {
                presenter?.removeFirstResponder()
            }
            return
        }
        self.firstName = firstName
        presenter?.switchToTextField(.lastName)
    }
    
    func setLastName(_ lastName: String) {
        if self.lastName != nil {
            if !(presenter?.incorrectValueForField(.birthday) ?? false) {
                presenter?.removeFirstResponder()
            }
            return
        }
        self.lastName = lastName
        presenter?.switchToTextField(.birthday)
    }
    
    func setBirthday(_ birthday: Date) {
        self.birthday = birthday
        presenter?.completeEntering()
    }
    
    func getFirstName() -> String? {
        return firstName
    }
    
    func getLastName() -> String? {
        return lastName
    }
    
    func getBirthday() -> Date? {
        return birthday
    }
    
    func firstEmptyField() -> FirstEnterField? {
        guard let firstName = firstName, !firstName.isEmpty else {
            return .firstName
        }
        guard let lastName = lastName, !lastName.isEmpty else {
            return .lastName
        }
        guard birthday != nil else {
            return .birthday
        }
        
        return nil
    }
    
    func sendUserInformation() {
        guard let firstName = firstName else {
            return
        }
        guard let lastName = lastName else {
            return
        }
        guard let birthday = birthday else {
            return
        }
        firstEnterRequest.perform(firstName: firstName, lastName: lastName, birthday: birthday, gender: UserGender.other)
    }
}

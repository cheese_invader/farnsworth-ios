//
//  FirstEnterInteractorProtocol.swift
//  Farnsworth
//
//  Created by Marty on 25/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol FirstEnterInteractorProtocol: class {
    func setFirstName(_ firstName: String)
    func setLastName(_ lastName: String)
    func setBirthday(_ birthday: Date)
    
    func getFirstName() -> String?
    func getLastName() -> String?
    func getBirthday() -> Date?
    
    func firstEmptyField() -> FirstEnterField?
    
    func sendUserInformation()
}

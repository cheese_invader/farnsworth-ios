//
//  ConfirmInteractor.swift
//  Farnsworth
//
//  Created by Marty on 24/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

class ConfirmInteractor {
    // VIPER
    private var router: ConfirmRouterProtocol?
    
    
    private var confirmRequest = ConfirmRequest()
    
    init() {
        confirmRequest.completionSuccess = { [weak self] in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.router?.confirmationSucceed()
            }
        }
        
        confirmRequest.completionFailure = { [weak self] (confirmError) in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.router?.confirmationFailed(confirmError)
            }
        }
    }
}

extension ConfirmInteractor: ConfirmInteractorProtocol {
    func setup(router: ConfirmRouterProtocol) {
        self.router = router
    }
    
    func confirm(loginToken: String, userId: String) {
        confirmRequest.perform(userId: userId, loginToken: loginToken)
    }
}

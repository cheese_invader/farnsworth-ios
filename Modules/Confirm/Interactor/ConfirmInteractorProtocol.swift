//
//  ConfirmInteractorProtocol.swift
//  Farnsworth
//
//  Created by Marty on 24/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol ConfirmInteractorProtocol: class {
    func setup(router: ConfirmRouterProtocol)
    func confirm(loginToken: String, userId: String)
}

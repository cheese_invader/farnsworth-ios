//
//  ConfirmError.swift
//  Farnsworth
//
//  Created by Marty on 15/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum ConfirmError: Error {
    case cantComposeJSON
    case notOkStatus(Int)
    case requestError(RequestError)
    case wrongResponse
    case undefined
    
    static func defineFromRequestError(_ requestError: RequestError) -> ConfirmError {
        return .requestError(requestError)
    }
    
    func description() -> String {
        switch self {
        case .requestError(let requestError):
            return requestError.description()
        case .notOkStatus(let status):
            switch status {
            case 500:
                return "Server error"
            case 404:
                return "Not found"
            case 400:
                return "Wrong request"
            case 403:
                return "Authentification error"
            default:
                return "Status is not 200"
            }
        default:
            return "Confirm error"
        }
    }
}

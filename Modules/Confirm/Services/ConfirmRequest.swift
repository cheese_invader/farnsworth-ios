//
//  ConfirmRequest.swift
//  Farnsworth
//
//  Created by Marty on 15/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

class ConfirmRequest {
    private var task: URLSessionDataTask?

    var url = Config.shared.serverURL.appendingPathComponent(Static.shared.apiConfirm)
    var completionSuccess: (() -> Void)?
    var completionFailure: ((ConfirmError) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeTaskWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                let requestError = RequestError.defineFromError(error)
                let confirmError = ConfirmError.defineFromRequestError(requestError)
                self.completionFailure?(confirmError)
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                self.completionFailure?(.wrongResponse)
                return
            }
            
            if response.statusCode != 200 {
                self.completionFailure?(.notOkStatus(response.statusCode))
                return
            }
            
            guard let data = data else {
                self.completionFailure?(.wrongResponse)
                return
            }
            
            guard let dataSerialized = try? JSONSerialization.jsonObject(with: data, options: []), let params = dataSerialized as? [String: String],
                let userId = params["userId"], let refreshToken = params["refreshToken"], let accessToken = params["accessToken"], let userStatusMessage = params["userStatus"] else {
                    self.completionFailure?(.wrongResponse)
                    return
            }
            
            let userStatus = UserStatus.defineByStatusMessage(userStatusMessage)
            
            Config.shared.userId       = userId
            Config.shared.refreshToken = refreshToken
            Config.shared.accessToken  = accessToken
            Config.shared.userStatus   = userStatus
            
            self.completionSuccess?()
        })
    }
}


// request
extension ConfirmRequest {
    func perform(userId: String, loginToken: String, completionSuccess: (() -> Void)?, completionFailure: ((ConfirmError) -> Void)?) {
        task?.cancel()
        
        self.completionSuccess = completionSuccess
        self.completionFailure = completionFailure
        
        perform(userId: userId, loginToken: loginToken)
    }
    
    func perform(userId: String, loginToken: String) {
        task?.cancel()
        
        let params = ["userId": userId, "loginToken": loginToken]
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
        } catch {
            completionFailure?(.cantComposeJSON)
            return
        }
        
        composeTaskWithRequest(request)
        
        task?.resume()
    }
}

//
//  ConfirmRouter.swift
//  Farnsworth
//
//  Created by Marty on 24/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

class ConfirmRouter {
    // VIPER
    private weak var interactor: ConfirmInteractorProtocol?
    
    private weak var navigation: NavigationViewController?
    
    init(interactor: ConfirmInteractorProtocol, navigationController: NavigationViewController) {
        self.interactor  = interactor
        self.navigation = navigationController
    }
}


extension ConfirmRouter: ConfirmRouterProtocol {
    static func assemble(embadedIn navigationController: NavigationViewController) -> ConfirmInteractor {
        let interactor = ConfirmInteractor()
        let router = ConfirmRouter(interactor: interactor, navigationController: navigationController)
        interactor.setup(router: router)
        
        return interactor
    }
    
    func confirmationSucceed() {
        navigation?.confirmationSucceed()
    }
    
    func confirmationFailed(_ error: ConfirmError) {
        navigation?.confirmationFailed(error)
    }
}

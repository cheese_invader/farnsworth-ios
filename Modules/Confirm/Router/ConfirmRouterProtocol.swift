//
//  ConfirmRouterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 24/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol ConfirmRouterProtocol: class {
    static func assemble(embadedIn navigationController: NavigationViewController) -> ConfirmInteractor
    func confirmationSucceed()
    func confirmationFailed(_ error: ConfirmError)
}

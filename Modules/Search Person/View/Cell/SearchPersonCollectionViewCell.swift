//
//  SearchPersonCollectionViewCell.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit


private let dateFormat = "yyyy/MM/dd HH:mm"


class SearchPersonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    func assembelFromSearchedUser(_ user: SearchedUserPayload) {
        self.nameLabel.text = user.firstName + " " + user.lastName
        if let username = user.username {
            self.usernameLabel.text = "@\(username)"
        } else {
            self.usernameLabel.text = ""
        }
        
        if user.status.status == .online {
            self.statusLabel.text = user.status.status.rawValue
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat
            self.statusLabel.text = "last seen " + formatter.string(from: user.status.lastSeen)
        }
    }
}

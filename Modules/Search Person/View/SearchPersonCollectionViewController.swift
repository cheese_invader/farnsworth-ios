//
//  SearchPersonCollectionViewController.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit


private let reuseIdentifier = "SearchPersonCell"
private let cellNibName     = "SearchPersonCollectionViewCell"


class SearchPersonCollectionViewController: UICollectionViewController {
    // VIPER
    private var presenter: SearchPersonPresenterProtocol?
    
    private let searchController = UISearchController(searchResultsController: nil)
    private var foundUsers = [SearchedUserPayload]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSearchBar()
        registerNib()
    }
}


extension SearchPersonCollectionViewController: SearchPersonCollectionViewControllerProtocol {
    func setup(presenter: SearchPersonPresenterProtocol) {
        self.presenter = presenter
    }
    
    func reload(users: FoundUsersPayload) {
        foundUsers = users.users
        collectionView.reloadData()
    }
}


// MARK: - UICollectionViewDataSource -
extension SearchPersonCollectionViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return foundUsers.count
    }
    
    
    // Cell creator
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SearchPersonCollectionViewCell
        
        cell.assembelFromSearchedUser(foundUsers[indexPath.row])
        
        return cell
    }
}


// MARK: - UICollectionViewDelegateFlowLayout -
extension SearchPersonCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth  = view.frame.size.width - 20
        let cellHeight: CGFloat = cellWidth / 3
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
}


// MARK: - Helper -
extension SearchPersonCollectionViewController {
    func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "full name or @username"
        searchController.searchBar.keyboardAppearance = .dark
        searchController.searchBar.tintColor  = #colorLiteral(red: 1, green: 0.7882352941, blue: 0, alpha: 1)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    func registerNib() {
        let nibCell = UINib(nibName: cellNibName, bundle: nil)
        self.collectionView!.register(nibCell, forCellWithReuseIdentifier: reuseIdentifier)
    }
}


// MARK: - Search controller -
extension SearchPersonCollectionViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {
            return
        }
        presenter?.searchStringWasChangedTo(searchText)
    }
}

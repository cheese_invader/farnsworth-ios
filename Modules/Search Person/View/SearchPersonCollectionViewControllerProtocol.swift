//
//  SearchPersonCollectionViewControllerProtocol.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol SearchPersonCollectionViewControllerProtocol: class {
    func setup(presenter: SearchPersonPresenterProtocol)
    
    func reload(users: FoundUsersPayload)
}

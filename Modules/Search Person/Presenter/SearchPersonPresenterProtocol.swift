//
//  SearchPersonPresenterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol SearchPersonPresenterProtocol: class {
    func setup(view: SearchPersonCollectionViewControllerProtocol, router: SearchPersonRouterProtocol, interactor: SearchPersonInteractorProtocol)
    
    // -> View
    func searchStringWasChangedTo(_ searchString: String)
    
    // -> Interactor
    func foundUsersAreComing(_ users: FoundUsersPayload)
}

//
//  SearchPersonPresenter.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation


class SearchPersonPresenter {
    // MARK: - VIPER -
    private weak var view : SearchPersonCollectionViewControllerProtocol?
    private var interactor: SearchPersonInteractorProtocol?
    private var router    : SearchPersonRouterProtocol?
}


extension SearchPersonPresenter: SearchPersonPresenterProtocol {
    func setup(view: SearchPersonCollectionViewControllerProtocol, router: SearchPersonRouterProtocol, interactor: SearchPersonInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    
    // MARK: - -> View -
    func searchStringWasChangedTo(_ searchString: String) {
        interactor?.searchString(searchString)
    }
    
    
    // MARK: - -> Interactor -
    func foundUsersAreComing(_ users: FoundUsersPayload) {
        view?.reload(users: users)
    }
}

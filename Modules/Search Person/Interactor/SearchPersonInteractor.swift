//
//  SearchPersonInteractor.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation


class SearchPersonInteractor {
    // VIPER
    private weak var presenter: SearchPersonPresenterProtocol?
    
    private let socket: FarnsworthSocketProtocol = FarnsworthSocket.shared
    private var searchString = ""
    private var wasUpdated = false
    private var isBusy = false
    
    init(presenter: SearchPersonPresenterProtocol) {
        self.presenter = presenter
        addConnectionUpdatedObserver()
        CentralRouter.shared.searchUsersDelegate = self
    }
    
    deinit {
        defer {
            CentralRouter.shared.searchUsersDelegate = nil
            removeConnectionUpdatedObserver()
        }
    }
    
    
    func sendSearchRequest() {
        if searchString.isEmpty {
            presenter?.foundUsersAreComing(FoundUsersPayload(users: []))
            return
        }
        if isBusy || !wasUpdated {
            return
        }
        
        let findUsersPayload = FindUsersPayload(search: searchString)
        guard
            let findUsersPayloadJson = try? JSONEncoder().encode(findUsersPayload),
            let payload = String(data: findUsersPayloadJson, encoding: .utf8)
        else {
            return
        }
        
        let findUsersInfo = FSOutcomingMessageSchema(messageType: .findUsers, payload: payload)
        guard let findUsersJSONInfo = try? JSONEncoder().encode(findUsersInfo) else {
            return
        }
        if !socket.isConnected {
            return
        }
        
        socket.sendMessage(findUsersJSONInfo) { [weak self] in
            guard let self = self else {
                return
            }
            self.isBusy = true
            self.wasUpdated = false
        }
    }
}


extension SearchPersonInteractor: SearchPersonInteractorProtocol {
    func searchString(_ searchString: String) {
        if !wasUpdated && self.searchString != searchString {
            wasUpdated = true
        }
        self.searchString = searchString
        sendSearchRequest()
    }
}

extension SearchPersonInteractor: WSSearchUsersDelegate {
    func usersWereFound(_ users: FoundUsersPayload) {
        print("update")
        isBusy = false
        presenter?.foundUsersAreComing(users)
        sendSearchRequest()
    }
}

// MARK: - Observers -
extension SearchPersonInteractor {
    private func addConnectionUpdatedObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(connectionUpdaterSuccessUpdate), name: Notification.Name.connectionUpdaterSuccessUpdate, object: nil)
    }
    
    private func removeConnectionUpdatedObserver() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.connectionUpdaterSuccessUpdate, object: nil)
    }
    
    @objc private func connectionUpdaterSuccessUpdate() {
        sendSearchRequest()
    }
}

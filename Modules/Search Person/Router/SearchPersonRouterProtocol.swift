//
//  SearchPersonRouterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol SearchPersonRouterProtocol: class {
    static func assemble(embadedIn navigationController: NavigationViewController) -> SearchPersonCollectionViewController
}

//
//  SearchPersonRouter.swift
//  Farnsworth
//
//  Created by Marty on 16/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit


private let storyboardName = "SearchPerson"
private let storyboardID   = "SearchPersonStoryboard"


class SearchPersonRouter {
    static let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
    
    private weak var navigation: NavigationViewController?
    
    
    // VIPER
    private weak var presenter: SearchPersonPresenterProtocol?
    
    
    init(presenter: SearchPersonPresenterProtocol, navigationController: NavigationViewController) {
        self.presenter = presenter
        self.navigation = navigationController
    }
}


extension SearchPersonRouter: SearchPersonRouterProtocol {
    static func assemble(embadedIn navigationController: NavigationViewController) -> SearchPersonCollectionViewController {
        let searchPersonVC = SearchPersonRouter.storyboard.instantiateViewController(withIdentifier: storyboardID) as! SearchPersonCollectionViewController
        
        let presenter  = SearchPersonPresenter()
        let interactor = SearchPersonInteractor(presenter: presenter)
        let router     = SearchPersonRouter(presenter: presenter, navigationController: navigationController)
        
        presenter .setup(view: searchPersonVC, router: router, interactor: interactor)
        searchPersonVC.setup(presenter: presenter)
        
        return searchPersonVC
    }
}

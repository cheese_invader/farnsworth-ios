//
//  LoginPresenterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 11/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol LoginPresenterProtocol: class {
    func setup(view: LoginViewControllerProtocol, router: LoginRouterProtocol, interactor: LoginInteractorProtocol)
    
    func changeSceneTo(_ scene: LoginScene)
    
    // View ->
    func signIn(email: String)
    func signInAgain(email: String)
    func openMail()
    func changeEmail()
    
    // Interactor ->
    func signInSuccess()
    func signInFailure(message: String)
    func setTimer(animated: Bool, minutes: Int, seconds: Int)
    func setTimerDanger()
    func timeOver()
}

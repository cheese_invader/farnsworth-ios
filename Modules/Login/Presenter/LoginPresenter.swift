//
//  LoginPresenter.swift
//  Farnsworth
//
//  Created by Marty on 11/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation


private let welcomeMessage = "Welcome to Farnsworth!"
private let   timerMessage = "Now, check your inbox"
private let   wrongMessage = "Semething was wrong"
private let expiredMessage = "Expired"
private let openMailButtonLabel = "Open mail app"

private let messagesDuration = 1.0
private let elementsDuration = 2.0


class LoginPresenter {
    // MARK: - VIPER -
    private weak var view : LoginViewControllerProtocol?
    private var interactor: LoginInteractorProtocol?
    private var router    : LoginRouterProtocol?
    
    
    private var scene = LoginScene.send
}


// MARK: - Scene switcher -

extension LoginPresenter {
    private func switchSendSceneToTimer() {
        view?.isTextFieldAndSendButtonEnabled = false
        
        view?.setMessageTitleAnimated(timerMessage, duration: messagesDuration, delay: 0)
        view?.upEmailTextField(duration: elementsDuration, delay: 0.5)
        
        view?.showTimer(duration: elementsDuration, delay: 0.8)
        
        view?.setSendButtonTitleAnimated(openMailButtonLabel, duration: 1, delay: 1)
        view?.changeButtonFromSendToMail(duration: elementsDuration, delay: 1)
    }
    
    private func switchSendSceneToExpired() {
        view?.isTextFieldAndSendButtonEnabled = false
        
        view?.setMessageTitleAnimated(wrongMessage, duration: messagesDuration, delay: 0)
        view?.upEmailTextField(duration: elementsDuration, delay: 0.5)
        
        view?.setTimerColor(.red)
        view?.setTimerTitle(expiredMessage)
        view?.showTimer(duration: elementsDuration, delay: 0.8)
        
        view?.setSendButtonTitleAnimated(openMailButtonLabel, duration: 1, delay: 1)
        view?.changeButtonFromSendToMail(duration: elementsDuration / 2, delay: 1)
        view?.changeButtonFromMailToExpired(duration: elementsDuration / 2, delay: elementsDuration / 2 + 1)
    }
    
    private func switchTimerSceneToSend() {
        // TODO: Implement
    }
    
    private func switchTimerSceneToExpired() {
        view?.setMessageTitleAnimated(wrongMessage, duration: messagesDuration, delay: 0)
        
        view?.setTimerColor(.red)
        view?.setTimerTitleAnimated(expiredMessage, duration: messagesDuration, delay: 0.5)
        
        view?.changeButtonFromMailToExpired(duration: elementsDuration, delay: 0.8)
    }
    
    private func switchExpiredSceneToSend() {
        view?.setMessageTitleAnimated(welcomeMessage, duration: messagesDuration, delay: 0)
        
        view?.hideTimer(duration: elementsDuration, delay: 0.5)
        view?.downEmailTextField(duration: elementsDuration, delay: 0.8)
        
        view?.changeButtonFromExpiredToSend(duration: elementsDuration, delay: 1)
    }
    
    private func switchExpiredSceneToTimer() {
        view?.setMessageTitleAnimated(timerMessage, duration: messagesDuration, delay: 0)
        view?.changeButtonFromExpiredToMail(duration: elementsDuration, delay: 0.5)
    }
}


extension LoginPresenter: LoginPresenterProtocol {
    func setup(view: LoginViewControllerProtocol, router: LoginRouterProtocol, interactor: LoginInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    
    func changeSceneTo(_ scene: LoginScene) {
        switch self.scene {
        case .send where scene == .timer:
            switchSendSceneToTimer()
        case .send where scene == .expired:
            switchSendSceneToExpired()
        case .timer where scene == .send:
            switchTimerSceneToSend()
        case .timer where scene == .expired:
            switchTimerSceneToExpired()
        case .expired where scene == .send:
            switchExpiredSceneToSend()
        case .expired where scene == .timer:
            switchExpiredSceneToTimer()
        default:
            return
        }
        self.scene = scene
    }
    
    
    // MARK: - View -> -
    func signIn(email: String) {
        if !email.isValidEmail() {
            view?.shakeEmailField()
            view?.unlockSignIn(clearTextField: true)
            return
        }
        view?.isLoginActivityIndicatorShown = true
        interactor?.signInWithEmail(email)
    }
    
    func signInAgain(email: String) {
        changeSceneTo(.expired)
        interactor?.signInWithEmail(email)
    }
    
    func openMail() {
        router?.openMailApp()
    }
    
    func changeEmail() {
        changeSceneTo(.send)
    }
    
    
    // MARK: - Interactor -> -
    func signInSuccess() {
        view?.isLoginActivityIndicatorShown = false
        changeSceneTo(.timer)
    }
    
    func signInFailure(message: String) {
        view?.isLoginActivityIndicatorShown = false
        view?.setMessageTitleAnimated(message, duration: 0.5, delay: 0)
        view?.unlockSignIn(clearTextField: false)
    }
    
    func setTimer(animated: Bool, minutes: Int, seconds: Int) {
        let title = String(format: "%01d:%02d", minutes, seconds)
        if animated {
            view?.setTimerTitleAnimated(title, duration: messagesDuration, delay: 0)
        } else {
            view?.setTimerTitle(title)
        }
    }
    
    func setTimerDanger() {
        view?.setTimerColor(.red)
    }
    
    func timeOver() {
        changeSceneTo(.expired)
    }
}

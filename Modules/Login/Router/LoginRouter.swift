//
//  LoginRouter.swift
//  Farnsworth
//
//  Created by Marty on 11/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "Login"
private let storyboardID   = "LoginStoryboard"


class LoginRouter {
    static let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
    
    private weak var navigation: NavigationViewController?
    
    
    // VIPER
    private weak var presenter: LoginPresenterProtocol?
    
    
    init(presenter: LoginPresenterProtocol, navigationController: NavigationViewController) {
        self.presenter  = presenter
        self.navigation = navigationController
    }
}


extension LoginRouter: LoginRouterProtocol {
    static func assemble(embadedIn navigationController: NavigationViewController) -> LoginViewController {
        let loginVC = LoginRouter.storyboard.instantiateViewController(withIdentifier: storyboardID) as! LoginViewController
        
        let presenter  = LoginPresenter()
        let interactor = LoginInteractor(presenter: presenter)
        let router     = LoginRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: loginVC, router: router, interactor: interactor)
        loginVC  .setup(presenter: presenter)
        
        return loginVC
    }
    
    func openMailApp() {
        let mailURL = URL(string: "message://")!
        
        if UIApplication.shared.canOpenURL(mailURL) {
            UIApplication.shared.open(mailURL, options: [:], completionHandler: nil)
        }
    }
}

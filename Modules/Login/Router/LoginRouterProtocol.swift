//
//  LoginRouterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 11/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol LoginRouterProtocol {
    static func assemble(embadedIn navigationController: NavigationViewController) -> LoginViewController
    
    func openMailApp()
}

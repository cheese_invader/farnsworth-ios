//
//  LoginScene.swift
//  Farnsworth
//
//  Created by Marty on 17/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum LoginScene {
    case send
    case timer
    case expired
}

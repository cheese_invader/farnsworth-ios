//
//  LoginError.swift
//  Farnsworth
//
//  Created by Marty on 14/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum LoginError: Error {
    case cantComposeJSON
    case notOkStatus(Int)
    case requestError(RequestError)
    case wrongResponse
    case undefined
    
    static func defineFromRequestError(_ requestError: RequestError) -> LoginError {
        return .requestError(requestError)
    }
    
    func description() -> String {
        switch self {
        case .requestError(let requestError):
            return requestError.description()
        default:
            return "Login error"
        }
    }
}

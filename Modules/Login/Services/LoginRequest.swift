//
//  LoginRequest.swift
//  Farnsworth
//
//  Created by Marty on 15/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

class LoginRequest {
    private var task: URLSessionDataTask?
    
    private var emailCandidate: String?
    
    var url = Config.shared.serverURL.appendingPathComponent(Static.shared.apiLogin)
    var device = Config.shared.device ?? Static.shared.baseDevice
    var completionSuccess: (() -> Void)?
    var completionFailure: ((LoginError) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeTaskWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                let requestError = RequestError.defineFromError(error)
                let loginError = LoginError.defineFromRequestError(requestError)
                self.completionFailure?(loginError)
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                self.completionFailure?(.wrongResponse)
                return
            }
            
            if response.statusCode != 200 {
                self.completionFailure?(.notOkStatus(response.statusCode))
                return
            }
            
            self.completionSuccess?()
        })
    }
}


// request
extension LoginRequest {
    func perform(email: String, completionSuccess: (() -> Void)?, completionFailure: ((LoginError) -> Void)?) {
        task?.cancel()
        
        self.completionSuccess = completionSuccess
        self.completionFailure = completionFailure
        
        perform(email: email)
    }
    
    func perform(email: String) {
        task?.cancel()
        emailCandidate = email
        
        let params = ["email": email, "device": device]
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
        } catch {
            self.completionFailure?(.cantComposeJSON)
        }
        
        composeTaskWithRequest(request)
        task?.resume()
    }
}

//
//  LoginViewController.swift
//  Farnsworth
//
//  Created by Marty on 11/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

private let appearDuration = 0.4

private let lineOffsetSide: CGFloat   = 50
private let lineOffsetBottom: CGFloat = 15

private let emailTextFieldEndConstraint: CGFloat = -100
private let          timerEndConstraint: CGFloat = -20

private let sendButtonLabel = "Send"


class LoginViewController: UIViewController {
    // MARK: - VIPER -
    private var presenter: LoginPresenterProtocol?
    
    
    // MARK: - Constraints -
    @IBOutlet weak var blurVisualEffectView: UIVisualEffectView!
    private var blurEffect: UIVisualEffect?
    
    @IBOutlet weak var emailTextFieldCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var          timerCenterConstraint: NSLayoutConstraint!
    
    var sendButtonToOpenMailWidthAnchor: NSLayoutConstraint!
    var sendButtonToExpiredWidthAnchor : NSLayoutConstraint!
    var openMailWidthAnchor            : NSLayoutConstraint!
    var sendAgainButtonWidthAnchor     : NSLayoutConstraint!
    var changeMailButtonWidthAnchor    : NSLayoutConstraint!
    
    
    // MARK: - Outlets -
    @IBOutlet weak var loginActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timerLabel  : UILabel!
    @IBOutlet weak var emailTextField: ShakeTextField!
    @IBOutlet weak var sendButton      : RoundButton!
    @IBOutlet weak var openMailButton  : RoundButton!
    @IBOutlet weak var sendAgainButton : RoundButton!
    @IBOutlet weak var changeMailButton: RoundButton!
    
    
    // MARK: - Actions -
    @IBAction func sendButtonWasTapped(_ sender: UIButton) {
        send()
    }
    
    @IBAction func openMailButtonWasTapped(_ sender: UIButton) {
        presenter?.openMail()
    }
    
    @IBAction func sendAgainButtonWasTapped(_ sender: UIButton) {
        guard let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            return
        }
        presenter?.signInAgain(email: email)
    }
    
    @IBAction func changeMailButtonWatTapped(_ sender: UIButton) {
        presenter?.changeEmail()
        emailTextField.isEnabled = true
    }
    
    @IBAction func keyboardSendButtonWasTapped(_ sender: ShakeTextField) {
        send()
    }
    
    
    // MARK: - VC stuff -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareForAppearing()
 
        setAnchors()
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        drawLineInCenter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: appearDuration) { [weak self] in
            guard let self = self else {
                return
            }
            self.sendButton.alpha = 1
            self.messageLabel.alpha = 1
            self.blurVisualEffectView.effect = self.blurEffect
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIView.animate(withDuration: appearDuration) { [weak self] in
            guard let self = self else {
                return
            }
            self.blurVisualEffectView.effect = nil
        }
    }
}


// MARK: - Helpers -

extension LoginViewController {
    private func prepareForAppearing() {
        blurEffect = blurVisualEffectView.effect
        blurVisualEffectView.effect = nil
        messageLabel.alpha = 0
        sendButton.alpha = 0
    }
    
    private func drawLineInCenter() {
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: lineOffsetSide, y: view.center.y + lineOffsetBottom))
        path.addLine(to: CGPoint(x: view.bounds.width - lineOffsetSide, y: view.center.y + lineOffsetBottom))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = #colorLiteral(red: 1, green: 0.7882352941, blue: 0, alpha: 1)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 2
        
        view.layer.addSublayer(shapeLayer)
    }
    
    private func setAnchors() {
        let expiredButtonsWidth = sendAgainButton.frame.maxX - changeMailButton.frame.minX
        
        sendButtonToOpenMailWidthAnchor = sendButton.widthAnchor.constraint(equalToConstant: openMailButton.bounds.width)
        sendButtonToExpiredWidthAnchor  = sendButton.widthAnchor.constraint(equalToConstant: expiredButtonsWidth)
        
        openMailWidthAnchor         = openMailButton  .widthAnchor.constraint(equalToConstant: expiredButtonsWidth)
        sendAgainButtonWidthAnchor  = sendAgainButton .widthAnchor.constraint(equalToConstant: expiredButtonsWidth)
        changeMailButtonWidthAnchor = changeMailButton.widthAnchor.constraint(equalToConstant: expiredButtonsWidth)
        
        sendAgainButtonWidthAnchor .isActive = true
        changeMailButtonWidthAnchor.isActive = true
    }
    
    private func send() {
        guard let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            return
        }
        
        emailTextField.text = email
        presenter?.signIn(email: email)
    }
}


extension LoginViewController: LoginViewControllerProtocol {
    func setup(presenter: LoginPresenterProtocol) {
        self.presenter = presenter
    }
    
    
    // MARK: - Sending locker -
    var isTextFieldAndSendButtonEnabled: Bool {
        get {
            return emailTextField.isEnabled
        }
        set {
            emailTextField.isEnabled = newValue
            sendButton    .isEnabled = newValue
        }
    }
    
    
    var isLoginActivityIndicatorShown: Bool {
        get {
            return loginActivityIndicator.isAnimating
        }
        set {
            if newValue {
                loginActivityIndicator.startAnimating()
            } else {
                loginActivityIndicator.stopAnimating()
            }
        }
    }
    
    
    // MARK: - Timer stuff -
    func setTimerTitle(_ title: String) {
        timerLabel.text = title
    }
    
    func setTimerColor(_ color: UIColor) {
        timerLabel.textColor = color
    }
    
    
    // MARK: - Animation titles -
    func setSendButtonTitleAnimated(_ title: String, duration: Double, delay: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            UIView.transition(with: strongSelf.sendButton, duration: duration, options: .transitionCrossDissolve, animations: { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.sendButton.setTitle(title, for: .normal)
                }, completion: nil)
        }
    }
    
    func setMessageTitleAnimated(_ title: String, duration: Double, delay: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            UIView.transition(with: strongSelf.messageLabel, duration: duration, options: .transitionCrossDissolve, animations: { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.messageLabel.text = title
                }, completion: nil)
        }
    }
    
    func setTimerTitleAnimated(_ title: String, duration: Double, delay: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            UIView.transition(with: strongSelf.timerLabel, duration: duration, options: .transitionCrossDissolve, animations: { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.timerLabel.text = title
                }, completion: nil)
        }
    }
    
    
    // MARK: - Animation text field movements -
    func upEmailTextField(duration: Double, delay: Double) {
        emailTextFieldCenterConstraint.constant = emailTextFieldEndConstraint
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    func downEmailTextField(duration: Double, delay: Double) {
        emailTextFieldCenterConstraint.constant = 0
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    
    // MARK: - Animation timer appearance  -
    func showTimer(duration: Double, delay: Double) {
        timerCenterConstraint.constant = timerEndConstraint
        timerLabel.alpha = 0
        timerLabel.isHidden = false
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.timerLabel.alpha = 1
            
            strongSelf.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    func hideTimer(duration: Double, delay: Double) {
        timerCenterConstraint.constant = 0
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.timerLabel.alpha = 0
            
            strongSelf.view.layoutIfNeeded()
            }, completion: { [weak self] (_) in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.timerLabel.isHidden = true
        })
    }
    
    
    // MARK: - Button transitions -
    func changeButtonFromSendToMail(duration: Double, delay: Double) {
        sendButtonToOpenMailWidthAnchor.isActive = true
        sendButton.isEnabled = false
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.view.layoutIfNeeded()
            }, completion:{ [weak self] (_) in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.openMailButton.isHidden = false
                strongSelf.sendButton    .isHidden = true
                strongSelf.openMailButton.isEnabled = true
                
                strongSelf.sendButton.setTitle(sendButtonLabel, for: .normal)
                
                strongSelf.sendButtonToOpenMailWidthAnchor.isActive = false
        })
    }
    
    func changeButtonFromMailToExpired(duration: Double, delay: Double) {
        let animationProportion = 7.0
        openMailButton.isEnabled = false
        openMailWidthAnchor.isActive = true
        
        UIView.animate(withDuration: duration / animationProportion, delay: delay, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.view.layoutIfNeeded()
            }, completion: { [weak self] (_) in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.openMailButton  .isHidden = true
                strongSelf.sendAgainButton .isHidden = false
                strongSelf.changeMailButton.isHidden = false
                
                strongSelf.sendAgainButtonWidthAnchor .isActive = false
                strongSelf.changeMailButtonWidthAnchor.isActive = false
                
                UIView.animate(withDuration: (duration / animationProportion) * (animationProportion - 1), delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
                    guard let strongSelf = self else {
                        return
                    }
                    
                    strongSelf.view.layoutIfNeeded()
                    }, completion:{ [weak self] (_) in
                        guard let strongSelf = self else {
                            return
                        }
                        strongSelf.openMailWidthAnchor.isActive = false
                        
                        strongSelf.sendAgainButton .isEnabled = true
                        strongSelf.changeMailButton.isEnabled = true
                })
        })
    }
    
    func changeButtonFromExpiredToMail(duration: Double, delay: Double) {
        let animationProportion = 7.0
        changeMailButton.isEnabled = false
        sendAgainButton .isEnabled = false
        
        changeMailButtonWidthAnchor.isActive = true
        sendAgainButtonWidthAnchor .isActive = true
        openMailWidthAnchor        .isActive = true
        
        UIView.animate(withDuration: duration / animationProportion, delay: delay, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.view.layoutIfNeeded()
            }, completion: { [weak self] (_) in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.openMailButton  .isHidden = false
                strongSelf.sendAgainButton .isHidden = true
                strongSelf.changeMailButton.isHidden = true
                
                strongSelf.openMailWidthAnchor.isActive = false
                
                UIView.animate(withDuration: (duration / animationProportion) * (animationProportion - 1), delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
                    guard let strongSelf = self else {
                        return
                    }
                    
                    strongSelf.view.layoutIfNeeded()
                    }, completion:{ [weak self] (_) in
                        guard let strongSelf = self else {
                            return
                        }
                        
                        strongSelf.openMailButton.isEnabled = true
                })
        })
    }

    func changeButtonFromExpiredToSend(duration: Double, delay: Double) {
        let animationProportion = 7.0
        changeMailButton.isEnabled = false
        sendAgainButton .isEnabled = false
        
        changeMailButtonWidthAnchor   .isActive = true
        sendAgainButtonWidthAnchor    .isActive = true
        sendButtonToExpiredWidthAnchor.isActive = true
        
        UIView.animate(withDuration: duration / animationProportion, delay: delay, options: .curveLinear, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.view.layoutIfNeeded()
            }, completion: { [weak self] (_) in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.sendButton      .isHidden = false
                strongSelf.sendAgainButton .isHidden = true
                strongSelf.changeMailButton.isHidden = true
                
                strongSelf.sendButtonToExpiredWidthAnchor.isActive = false
                
                UIView.animate(withDuration: (duration / animationProportion) * (animationProportion - 1), delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
                    guard let strongSelf = self else {
                        return
                    }
                    
                    strongSelf.view.layoutIfNeeded()
                    }, completion:{ [weak self] (_) in
                        guard let strongSelf = self else {
                            return
                        }
                        
                        strongSelf.sendButton.isEnabled = true
                })
        })
    }
    

    func shakeEmailField() {
        emailTextField.shake()
    }
    
    func unlockSignIn(clearTextField: Bool) {
        if clearTextField {
            emailTextField.text = ""
        }
        emailTextField.isEnabled = true
        sendButton    .isEnabled = true
    }
}

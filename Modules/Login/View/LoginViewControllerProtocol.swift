//
//  LoginViewControllerProtocol.swift
//  Farnsworth
//
//  Created by Marty on 11/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewControllerProtocol: class {
    func setup(presenter: LoginPresenterProtocol)
    
    // Sending locker
    var isTextFieldAndSendButtonEnabled: Bool { get set }
    
    var isLoginActivityIndicatorShown: Bool { get set }
        
    // Timer stuff
    func setTimerTitle(_ title: String)
    func setTimerColor(_ color: UIColor)
    
    // Animation titles
    func setSendButtonTitleAnimated(_ title: String, duration: Double, delay: Double)
    func    setMessageTitleAnimated(_ title: String, duration: Double, delay: Double)
    func      setTimerTitleAnimated(_ title: String, duration: Double, delay: Double)
    
    // Animation text field movements
    func   upEmailTextField(duration: Double, delay: Double)
    func downEmailTextField(duration: Double, delay: Double)
    
    // Animation timer appearance
    func showTimer(duration: Double, delay: Double)
    func hideTimer(duration: Double, delay: Double)
    
    // Button transitions
    func changeButtonFromSendToMail   (duration: Double, delay: Double)
    func changeButtonFromMailToExpired(duration: Double, delay: Double)
    func changeButtonFromExpiredToMail(duration: Double, delay: Double)
    func changeButtonFromExpiredToSend(duration: Double, delay: Double)
    
    func shakeEmailField()
    func unlockSignIn(clearTextField: Bool)
}

//
//  LoginInteractor.swift
//  Farnsworth
//
//  Created by Marty on 11/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation


private let secondsInMinute   = 60
private let dangerSeconds     = 30


class LoginInteractor {
    // VIPER
    private weak var presenter: LoginPresenterProtocol?
    
    
    private let loginRequest = LoginRequest()
    
    
    // Timer
    private var deadline: Date!
    private var timer = Timer()
    private var isDanger = false
    
    
    init(presenter: LoginPresenterProtocol) {
        self.presenter = presenter
    }
}


// MARK: - Timer -

extension LoginInteractor {
    @objc private func ticTac() {
        let difference = Int(deadline.timeIntervalSinceNow)
        
        if difference < dangerSeconds && !isDanger {
            presenter?.setTimerDanger()
            isDanger = true
        }
        if difference <= 0 {
            stopTimer()
            presenter?.timeOver()
            return
        }
        
        presenter?.setTimer(animated: false, minutes: difference / secondsInMinute, seconds: difference % secondsInMinute)
    }
    
    private func stopTimer() {
        timer.invalidate()
    }
}


extension LoginInteractor: LoginInteractorProtocol {
    func startTimer() {
        deadline = Calendar.current.date(byAdding: .minute, value: Static.shared.minutesToConfirmDeadline, to: Date())
        
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(ticTac), userInfo: nil, repeats: true)
        presenter?.setTimer(animated: true, minutes: Static.shared.minutesToConfirmDeadline, seconds: 0)
    }
    
    func signInWithEmail(_ email: String) {
        loginRequest.completionSuccess = { [weak self] in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.startTimer()
                self.presenter?.signInSuccess()
            }
        }
        
        loginRequest.completionFailure = { [weak self] (loginError) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.signInFailure(message: loginError.description())
            }
        }
        
        loginRequest.perform(email: email)
    }
}

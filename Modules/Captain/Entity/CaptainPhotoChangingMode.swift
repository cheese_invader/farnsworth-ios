//
//  CaptainPhotoChangingMode.swift
//  Farnsworth
//
//  Created by Marty on 21/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum CaptainPhotoChangingMode {
    case showing
    case editing
    case choising
}

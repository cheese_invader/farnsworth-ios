//
//  CaptainRouterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 19/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol CaptainRouterProtocol: class {
    static func assemble(embadedIn navigationController: NavigationViewController) -> CaptainCollectionViewController
    func presentImagePicker(_ picker: FSImagePickerProtocol)
    func dismiss()
}

//
//  CaptainRouter.swift
//  Farnsworth
//
//  Created by Marty on 19/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit


private let storyboardName = "Captain"
private let storyboardID   = "CaptainStoryboard"


class CaptainRouter {
    static let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
    
    private weak var navigation: NavigationViewController?
    
    
    // VIPER
    private weak var presenter: CaptainPresenterProtocol?
    
    
    init(presenter: CaptainPresenterProtocol, navigationController: NavigationViewController) {
        self.presenter = presenter
        self.navigation = navigationController
    }
}


extension CaptainRouter: CaptainRouterProtocol {
    static func assemble(embadedIn navigationController: NavigationViewController) -> CaptainCollectionViewController {
        let captainVC = CaptainRouter.storyboard.instantiateViewController(withIdentifier: storyboardID) as! CaptainCollectionViewController
        
        let presenter  = CaptainPresenter()
        let interactor = CaptainInteractor(presenter: presenter)
        let router     = CaptainRouter(presenter: presenter, navigationController: navigationController)
        
        presenter .setup(view: captainVC, router: router, interactor: interactor)
        captainVC.setup(presenter: presenter)
        
        return captainVC
    }
    
    func presentImagePicker(_ picker: FSImagePickerProtocol) {
        picker.setNavigation(navigation)
        picker.takeImage()
    }
    
    func dismiss() {
        navigation?.popViewController(animated: true)
    }
}

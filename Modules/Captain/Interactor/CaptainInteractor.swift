//
//  CaptainInteractor.swift
//  Farnsworth
//
//  Created by Marty on 19/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit


class CaptainInteractor {
    // VIPER
    private weak var presenter: CaptainPresenterProtocol?
    
    private let socket: FarnsworthSocketProtocol = FarnsworthSocket.shared
    private let imagePicker = FSImagePicker()
    
    private var person = Person(id: "0", version: 0)
    private var oldPerson: Person?
    
    
    init(presenter: CaptainPresenterProtocol) {
        self.presenter = presenter
        addCaptainsInfoWasUpdatedObserver()
        
        imagePicker.setDelegate(self)
        setupPerson()
    }
    
    deinit {
        removeCaptainsInfoWasUpdatedObserver()
    }
}


extension CaptainInteractor: CaptainInteractorProtocol {
    func firstNameWasChangedTo(_ firstName: String) {
        person.update(firstName: firstName)
    }
    
    func lastNameWasChangedTo(_ lastName: String) {
       person.update(lastName: lastName)
    }
    
    func usernameWasChangedTo(_ username: String) {
        person.update(username: username)
    }
    
    func bioWasChangedTo(_ bio: String) {
        person.update(bio: bio)
    }
    
    func userWantsToUpdatePhotoFrom(_ source: UIImagePickerController.SourceType) {
        imagePicker.setSource(source)
        presenter?.imagePickerIsReadyForPresentation(imagePicker)
    }
    
    func getPerson() -> Person {
        return person
    }
    
    func sendUpdatedInfoToServer() {
        guard let oldPerson = self.oldPerson, person.completeness == .hihg else {
            return
        }
        let myInfoPayload = UpdateMyInfoPayload(firstName: person.firstName != oldPerson.firstName ? person.firstName        : nil,
                                                lastName : person.lastName  != oldPerson.lastName  ? person.lastName         : nil,
                                                gender   : person.gender    != oldPerson.gender    ? person.gender.rawValue  : nil,
                                                username : person.username  != oldPerson.username  ? person.username         : nil,
                                                bio      : person.bio       != oldPerson.bio       ? person.bio              : nil,
                                                photo    : person.photo     != oldPerson.photo     ? person.photo?.pngData() : nil)
        
        guard
            let myInfoPayloadJSON = try? JSONEncoder().encode(myInfoPayload),
            let paylod = String(data: myInfoPayloadJSON, encoding: .utf8)
        else {
            return
        }
        let updatedInfo = FSOutcomingMessageSchema(messageType: .updateMyInfo, payload: paylod)
        guard let info = try? JSONEncoder().encode(updatedInfo) else {
            return
        }
        
        socket.sendMessage(info) { [weak self] in
            guard let self = self else {
                return
            }
            self.presenter?.updateRequestWasSend()
        }
    }
}


extension CaptainInteractor {
    private func setupPerson() {
        CoreDataStackManager.shared.performTaskAsync({ [weak self] (context) in
            guard let self = self else {
                return
            }
            
            do {
                guard
                    let id = Config.shared.userId,
                    let rawCap = try FSPerson.findById(id, in: context),
                    let cap = Person.makeFromFSCDEntity(rawCap)
                else {
                    return
                }
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else {
                        return
                    }
                    
                    self.setupCaptain(cap)
                }
            } catch {
                assertionFailure("CaptainInteractor.setupPerson - setup problem -")
                return
            }
        }, completion: nil)
    }
    
    private func setupCaptain(_ cap: Person) {
        self.oldPerson = Person(id: cap.id, version: cap.version)
        if cap.completeness != .low {
            self.oldPerson?.setInfo(firstName: cap.firstName, lastName: cap.lastName, gender: cap.gender, status: cap.status, lastSeen: cap.lastSeen, username: cap.username, bio: cap.bio)
            
            if cap.completeness != .middle {
                self.oldPerson?.setPhoto(cap.photo)
            }
        }
        
        self.person = cap
        self.presenter?.personWasChanged()
    }
}


extension CaptainInteractor: FSImagePickerDelegate {
    func gotImage(_ image: UIImage) {
        guard person.completeness != .low else {
            return
        }
        person.setPhoto(image)
        presenter?.personWasChanged()
    }
}

// MARK: - Notification -
extension CaptainInteractor {
    private func addCaptainsInfoWasUpdatedObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(captainsInfoWasUpdated), name: Notification.Name.captainsInfoWasUpdated, object: nil)
    }
    
    private func removeCaptainsInfoWasUpdatedObserver() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.captainsInfoWasUpdated, object: nil)
    }
    
    @objc private func captainsInfoWasUpdated(notification: Notification) {
        guard let payload = notification.userInfo?["info"] as? Person else {
            return
        }
        setupCaptain(payload)
    }
}

//
//  CaptainMainInfoCollectionViewCell.swift
//  Farnsworth
//
//  Created by Marty on 19/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class CaptainMainInfoCollectionViewCell: UICollectionViewCell {
    weak var delegate: CaptainCellDelegate?
    
    // Outlets
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var takePhotoButton: RoundButton!
    @IBOutlet weak var openLibraryButton: RoundButton!
    @IBOutlet weak var changeImageButton: UIButton!
    
    
    // Actions
    @IBAction func firstNameEndEditing(_ sender: UITextField) {
        guard let firstName = firstNameTextField.text else {
            return
        }
        delegate?.firstNameWasChangedTo(firstName)
    }
    
    @IBAction func lastNameEndEditing(_ sender: UITextField) {
        guard let lastName = lastNameTextField.text else {
            return
        }
        delegate?.lastNameWasChangedTo(lastName)
    }
    
    @IBAction func changeImmageButtonWasTapped(_ sender: UIButton) {
        setImageMode(.choising)
    }
    
    @IBAction func takePhotoButtonWasTapped(_ sender: RoundButton) {
        delegate?.userWantsToUpdatePhotoFrom(.camera)
        setImageMode(.editing)
    }
    
    @IBAction func openLibraryButtonWasTapped(_ sender: RoundButton) {
        delegate?.userWantsToUpdatePhotoFrom(.photoLibrary)
        setImageMode(.editing)
    }
    
    
    // Init stuff
    override func awakeFromNib() {
        isEditable = false
        photo.image = UIImage(named: "Empty_user_photo")
        firstNameTextField.text = ""
        lastNameTextField.text = ""
        setImageMode(.showing)
    }
    
    func assembleFromPerson(_ person: Person) {
        guard person.completeness != .low else {
            return
        }
        
        firstNameTextField.text = person.firstName
        lastNameTextField.text  = person.lastName
        
        if person.completeness == .hihg {
            if let userPhoto = person.photo {
                photo.image = userPhoto
            }
        }
    }
    
    func setImageMode(_ mode: CaptainPhotoChangingMode) {
        switch mode {
        case .showing:
            photo.isHidden = false
            changeImageButton.isHidden = true
            openLibraryButton.isHidden = true
            takePhotoButton  .isHidden = true
        case .editing:
            photo.isHidden = false
            changeImageButton.isHidden = false
            openLibraryButton.isHidden = true
            takePhotoButton  .isHidden = true
        case .choising:
            photo.isHidden = true
            changeImageButton.isHidden = true
            openLibraryButton.isHidden = false
            takePhotoButton  .isHidden = false
        }
    }
}

extension CaptainMainInfoCollectionViewCell: CaptainCell {
    var isEditable: Bool {
        get {
            return firstNameTextField.isEnabled
        }
        set {
            firstNameTextField.isEnabled = newValue
            lastNameTextField .isEnabled = newValue
            let imageMode = newValue ? CaptainPhotoChangingMode.editing : .showing
            setImageMode(imageMode)
        }
    }
}

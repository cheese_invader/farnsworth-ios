//
//  CaptainBioCollectionViewCell.swift
//  Farnsworth
//
//  Created by Marty on 20/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class CaptainBioCollectionViewCell: UICollectionViewCell {
    weak var delegate: CaptainCellDelegate?
    
    
    @IBOutlet weak var bioTextView: UITextView!
    
    override func awakeFromNib() {
        bioTextView.text = ""
        isEditable = false
        bioTextView.delegate = self
    }
    
    func assembleFromPerson(_ person: Person) {
        guard person.completeness != .low else {
            return
        }
        
        bioTextView.text = person.bio
    }
}

extension CaptainBioCollectionViewCell: CaptainCell {
    var isEditable: Bool {
        get {
            return bioTextView.isEditable
        }
        set {
            bioTextView.isEditable = newValue
        }
    }
}

extension CaptainBioCollectionViewCell: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        delegate?.bioWasChangedTo(textView.text)
    }
}

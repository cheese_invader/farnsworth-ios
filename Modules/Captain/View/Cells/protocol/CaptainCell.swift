//
//  CaptainCell.swift
//  Farnsworth
//
//  Created by Marty on 20/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol CaptainCell {
    var isEditable: Bool { get set }
}

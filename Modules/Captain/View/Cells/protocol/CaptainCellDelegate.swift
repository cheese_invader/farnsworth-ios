//
//  CaptainCellDelegate.swift
//  Farnsworth
//
//  Created by Marty on 20/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

protocol CaptainCellDelegate: class {
    func firstNameWasChangedTo(_ firstName: String)
    func lastNameWasChangedTo(_ lastName: String)
    func usernameWasChangedTo(_ username: String)
    func bioWasChangedTo(_ bio: String)
    func userWantsToUpdatePhotoFrom(_ source: UIImagePickerController.SourceType)
}

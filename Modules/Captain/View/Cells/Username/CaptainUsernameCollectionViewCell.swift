//
//  CaptainUsernameCollectionViewCell.swift
//  Farnsworth
//
//  Created by Marty on 20/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class CaptainUsernameCollectionViewCell: UICollectionViewCell {
    weak var delegate: CaptainCellDelegate?
    
    
    // Outlets
    @IBOutlet weak var usernameTextField: UITextField!
    
    
    // Actions
    @IBAction func usernameEndEditing(_ sender: UITextField) {
        guard let username = usernameTextField.text else {
            return
        }
        delegate?.usernameWasChangedTo(username)
    }
    
    
    // Init stuff
    override func awakeFromNib() {
        usernameTextField.text = ""
        isEditable = false
    }
    
    func assembleFromPerson(_ person: Person) {
        guard person.completeness != .low else {
            return
        }
        
        usernameTextField.text = person.username ?? ""
    }
}

extension CaptainUsernameCollectionViewCell: CaptainCell {
    var isEditable: Bool {
        get {
            return usernameTextField.isEnabled
        }
        set {
            usernameTextField.isEnabled = newValue
        }
    }
    
    
}

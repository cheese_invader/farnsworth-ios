//
//  CaptainCollectionViewController.swift
//  Farnsworth
//
//  Created by Marty on 19/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit


private let mainInfoCellId  = "MainInfoCell"
private let mainInfoNibName = "CaptainMainInfoCollectionViewCell"

private let usernameCellId  = "UsernameCell"
private let usernameNibName = "CaptainUsernameCollectionViewCell"

private let bioCellId  = "BioCell"
private let bioNibName = "CaptainBioCollectionViewCell"

private let exitCellId  = "ExitCell"
private let exitNibName = "CaptainExitCollectionViewCell"

private let captainListsCellId  = "CaptainListsCell"
private let captainListsNibName = "CaptainListsCollectionViewCell"

private let mainInfoHeight: CGFloat  = 130
private let usernameHeight: CGFloat  = 50
private let      bioHeight: CGFloat  = 200

private let cellsCount = 5


class CaptainCollectionViewController: UICollectionViewController {
    // VIPER
    private var presenter: CaptainPresenterProtocol?
    
    
    private var isEditable = false
    
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    
    @IBAction func editButtonWasTapped(_ sender: UIBarButtonItem) {
        if isEditable {
            editButton.isEnabled = false
            presenter?.endEditingCaptainInfo()
        }
        isEditable = !isEditable
        setEditable(isEditable)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        registerNibs()
        setEditable(isEditable)
    }
}


extension CaptainCollectionViewController: CaptainCollectionViewControllerProtocol {
    func setup(presenter: CaptainPresenterProtocol) {
        self.presenter = presenter
    }
    
    func updateView() {
        collectionView.reloadData()
    }
}


// MARK: - CaptainCellDelegate -
extension CaptainCollectionViewController: CaptainCellDelegate {
    func firstNameWasChangedTo(_ firstName: String) {
        presenter?.firstNameWasChangedTo(firstName)
    }
    
    func lastNameWasChangedTo(_ lastName: String) {
        presenter?.lastNameWasChangedTo(lastName)
    }
    
    func usernameWasChangedTo(_ username: String) {
        presenter?.usernameWasChangedTo(username)
    }
    
    func bioWasChangedTo(_ bio: String) {
        presenter?.bioWasChangedTo(bio)
    }
    
    func userWantsToUpdatePhotoFrom(_ source: UIImagePickerController.SourceType) {
        presenter?.userWantsToUpdatePhotoFrom(source)
    }
}


// MARK: - Helpers -
extension CaptainCollectionViewController {
    private func registerNibs() {
        let mainInfoNibCell = UINib(nibName: mainInfoNibName, bundle: nil)
        self.collectionView!.register(mainInfoNibCell, forCellWithReuseIdentifier: mainInfoCellId)
        
        let usernameNibCell = UINib(nibName: usernameNibName, bundle: nil)
        self.collectionView!.register(usernameNibCell, forCellWithReuseIdentifier: usernameCellId)
        
        let bioNibCell = UINib(nibName: bioNibName, bundle: nil)
        self.collectionView!.register(bioNibCell, forCellWithReuseIdentifier: bioCellId)
        
        let exitNibCell = UINib(nibName: exitNibName, bundle: nil)
        self.collectionView!.register(exitNibCell, forCellWithReuseIdentifier: exitCellId)
        
        let captainListsCell = UINib(nibName: captainListsNibName, bundle: nil)
        self.collectionView.register(captainListsCell, forCellWithReuseIdentifier: captainListsCellId)
    }
    
    private func setEditable(_ isEditable: Bool) {
        editButton.title = isEditable ? "Done" : "Edit"
        
        for cell in collectionView.visibleCells {
            if var currentCell = cell as? CaptainCell {
                currentCell.isEditable = isEditable
            }
        }
    }
}


// MARK: - UICollectionViewDataSource -
extension CaptainCollectionViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellsCount
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let person = presenter?.getPerson() else {
            assertionFailure("CaptainCollectionViewController - Can't get person -")
            return UICollectionViewCell()
        }
        
        var cell: UICollectionViewCell!
        
        switch indexPath.row {
        case 0:
            let mainInfoCell = collectionView.dequeueReusableCell(withReuseIdentifier: mainInfoCellId, for: indexPath) as! CaptainMainInfoCollectionViewCell
            mainInfoCell.assembleFromPerson(person)
            mainInfoCell.delegate = self
            cell = mainInfoCell
        case 1:
            let usernameCell = collectionView.dequeueReusableCell(withReuseIdentifier: usernameCellId, for: indexPath) as! CaptainUsernameCollectionViewCell
            usernameCell.assembleFromPerson(person)
            usernameCell.delegate = self
            cell = usernameCell
        case 2:
            let bioCell = collectionView.dequeueReusableCell(withReuseIdentifier: bioCellId, for: indexPath) as! CaptainBioCollectionViewCell
            bioCell.assembleFromPerson(person)
            bioCell.delegate = self
            cell = bioCell
        case 3:
            let captainListsCell = collectionView.dequeueReusableCell(withReuseIdentifier: captainListsCellId, for: indexPath) as! CaptainListsCollectionViewCell
            cell = captainListsCell
        case 4:
            let logOutCell = collectionView.dequeueReusableCell(withReuseIdentifier: exitCellId, for: indexPath) as! CaptainExitCollectionViewCell
            cell = logOutCell
        default:
            assertionFailure("CaptainCollectionViewController - incorrect cell number -")
        }
        
        return cell
    }
}


// MARK: - UICollectionViewDelegateFlowLayout -
extension CaptainCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth  = view.frame.size.width - 20
        let cellHeight: CGFloat!
        
        switch indexPath.row {
        case 0:
            cellHeight = mainInfoHeight
        case 1, 4:
            cellHeight = usernameHeight
        case 2:
            cellHeight = bioHeight
        case 3:
            cellHeight = 120
        default:
            cellHeight = 100
            assertionFailure("CaptainCollectionViewController - incorrect cell number -")
        }
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

//
//  CaptainCollectionViewControllerProtocol.swift
//  Farnsworth
//
//  Created by Marty on 19/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol CaptainCollectionViewControllerProtocol: class {
    func setup(presenter: CaptainPresenterProtocol)
    func updateView()
}

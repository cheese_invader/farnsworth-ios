//
//  CaptainPresenterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 19/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

protocol CaptainPresenterProtocol: class {
    func setup(view: CaptainCollectionViewControllerProtocol, router: CaptainRouterProtocol, interactor: CaptainInteractorProtocol)
    
    // -> View
    func firstNameWasChangedTo(_ firstName: String)
    func lastNameWasChangedTo(_ lastName: String)
    func usernameWasChangedTo(_ username: String)
    func bioWasChangedTo(_ bio: String)
    func userWantsToUpdatePhotoFrom(_ source: UIImagePickerController.SourceType)
    func endEditingCaptainInfo()
    
    func getPerson() -> Person?
    
    // -> Interactor
    func imagePickerIsReadyForPresentation(_ picker: FSImagePickerProtocol)
    func personWasChanged()
    func updateRequestWasSend()
}

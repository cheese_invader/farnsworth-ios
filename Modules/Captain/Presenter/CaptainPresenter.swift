//
//  CaptainPresenter.swift
//  Farnsworth
//
//  Created by Marty on 19/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit


class CaptainPresenter {
    // MARK: - VIPER -
    private weak var view : CaptainCollectionViewControllerProtocol?
    private var interactor: CaptainInteractorProtocol?
    private var router    : CaptainRouterProtocol?
}


extension CaptainPresenter: CaptainPresenterProtocol {
    func setup(view: CaptainCollectionViewControllerProtocol, router: CaptainRouterProtocol, interactor: CaptainInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    func firstNameWasChangedTo(_ firstName: String) {
        interactor?.firstNameWasChangedTo(firstName)
    }
    
    func lastNameWasChangedTo(_ lastName: String) {
        interactor?.lastNameWasChangedTo(lastName)
    }
    
    func usernameWasChangedTo(_ username: String) {
        interactor?.usernameWasChangedTo(username)
    }
    
    func bioWasChangedTo(_ bio: String) {
        interactor?.bioWasChangedTo(bio)
    }
    
    func userWantsToUpdatePhotoFrom(_ source: UIImagePickerController.SourceType) {
        interactor?.userWantsToUpdatePhotoFrom(source)
    }
    
    func endEditingCaptainInfo() {
        interactor?.sendUpdatedInfoToServer()
    }
    
    func getPerson() -> Person? {
        return interactor?.getPerson()
    }
    
    
    func imagePickerIsReadyForPresentation(_ picker: FSImagePickerProtocol) {
        router?.presentImagePicker(picker)
    }
    
    func personWasChanged() {
        view?.updateView()
    }
    
    func updateRequestWasSend() {
        router?.dismiss()
    }
}

//
//  UsersPhotoReceiver.swift
//  Farnsworth
//
//  Created by Marty on 30/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class UsersPhotoReceiver {
    init() {
        addGotUsersPhotoObserver()
    }
    
    deinit {
        removeGotUsersPhotoObserver()
    }
}

// MARK: - Notifications -
extension UsersPhotoReceiver {
    private func addGotUsersPhotoObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(gotUsersPhoto), name: Notification.Name.gotUsersPhoto, object: nil)
    }
    
    private func removeGotUsersPhotoObserver() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.gotUsersPhoto, object: nil)
    }
    
    @objc private func gotUsersPhoto(notification: Notification) {
        guard let payload = notification.userInfo?["info"] as? UsersPhotoPayload else {
            return
        }
        
        CoreDataStackManager.shared.performTaskAsync({ (context) in
            do {
                guard
                    let rawUser = try FSPerson.findById(payload.id, in: context),
                    let user = Person.makeFromFSCDEntity(rawUser)
                else {
                    assertionFailure("UsersPhotoReceiver.gotUsersPhoto - find user problem -")
                    return
                }
                var photo: UIImage?
                if let rawPhoto = payload.photo {
                    guard let photoData = Data(base64Encoded: rawPhoto) else {
                        assertionFailure("UsersPhotoReceiver.gotUsersPhoto - bad encoding -")
                        return
                    }
                    photo = UIImage(data: photoData)
                }
                
                if user.completeness == .middle {
                    user.setPhoto(photo)
                }
                FSPerson.rewrite(user, in: context)
                if let id = Config.shared.userId, user.id == id {
                    self.sendCaptainsInfoWasUpdatedNotification(user)
                }
                try context.save()
            } catch {
                assertionFailure("UsersPhotoReceiver.gotUsersPhoto - find user problem -")
                return
            }
            
        }, completion: nil)
    }
}

extension UsersPhotoReceiver {
    private func sendCaptainsInfoWasUpdatedNotification(_ captain: Person) {
        NotificationCenter.default.post(name: Notification.Name.captainsInfoWasUpdated, object: nil, userInfo: ["info": captain])
    }
}

//
//  ChatListRouter.swift
//  Farnsworth
//
//  Created by Marty on 13/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit


private let storyboardName = "ChatList"
private let storyboardID   = "ChatListStoryboard"


class ChatListRouter {
    static let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
    
    private weak var navigation: NavigationViewController?
    
    
    // VIPER
    private weak var presenter: ChatListPresenterProtocol?
    
    
    init(presenter: ChatListPresenterProtocol, navigationController: NavigationViewController) {
        self.presenter  = presenter
        self.navigation = navigationController
    }
}


extension ChatListRouter: ChatListRouterProtocol {
    static func assemble(embadedIn navigationController: NavigationViewController) -> ChatListCollectionViewController {
        let chatListVC = ChatListRouter.storyboard.instantiateViewController(withIdentifier: storyboardID) as! ChatListCollectionViewController
        
        let presenter  = ChatListPresenter()
        let interactor = ChatListInteractor(presenter: presenter)
        let router     = ChatListRouter(presenter: presenter, navigationController: navigationController)
        
        presenter .setup(view: chatListVC, router: router, interactor: interactor)
        chatListVC.setup(presenter: presenter)
        
        return chatListVC
    }
    
    var isNavbarHidden: Bool {
        get {
            return navigation?.isNavigationBarHidden ?? true
        }
        set {
            navigation?.setNavigationBarHidden(newValue, animated: true)
        }
    }
    
    func creationChatFailure(error: FError) {
        navigation?.showAlert(title: error.classDescription, message: error.detailDescription)
    }
    
    func showSearchPersonModule() {
        guard let navigation = navigation else {
            return
        }
        let searchPersonVC = SearchPersonRouter.assemble(embadedIn: navigation)
        navigation.pushViewController(searchPersonVC, animated: true)
    }
    
    func pushCaptainModule() {
        guard let navigation = navigation else {
            return
        }
        let captainVC = CaptainRouter.assemble(embadedIn: navigation)
        navigation.pushViewController(captainVC, animated: true)
    }
}

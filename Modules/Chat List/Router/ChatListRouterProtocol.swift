//
//  ChatListRouterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 13/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol ChatListRouterProtocol: class {
    static func assemble(embadedIn navigationController: NavigationViewController) -> ChatListCollectionViewController
    
    var isNavbarHidden: Bool { get set }
    func creationChatFailure(error: FError)
    func showSearchPersonModule()
    func pushCaptainModule()
}

//
//  ChatListPresenter.swift
//  Farnsworth
//
//  Created by Marty on 13/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit


class ChatListPresenter {
    // MARK: - VIPER -
    private weak var view : ChatListCollectionViewControllerProtocol?
    private var interactor: ChatListInteractorProtocol?
    private var router    : ChatListRouterProtocol?
}


extension ChatListPresenter: ChatListPresenterProtocol {
    func setup(view: ChatListCollectionViewControllerProtocol, router: ChatListRouterProtocol, interactor: ChatListInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    // MARK: - -> View -
    func readyToConnect() {
        view?.setTitle("Connection...")
        interactor?.readyToConnect()
    }
    
    func createNewChatButtonWasTapped() {
        router?.isNavbarHidden = true
        view?.showCreateChatPopup()
    }
    
    func createChatCancelButtonWasTapped() {
        router?.isNavbarHidden = false
        view?.setCreateChatInProgress = false
        view?.hideCreateChatPopover()
    }
    
    func userWantsToCreateChat(_ name: String) {
        if name.isEmpty {
            view?.shakeCreateChatPopoverTextField()
            return
        }
        router?.isNavbarHidden = true
        view?.setCreateChatInProgress = true
        interactor?.createNewChatRequest(title: name)
    }
    
    func userWantsToSearchPerson() {
        router?.showSearchPersonModule()
    }
    
    func userWantsToSeeCaptainsInfo() {
        router?.pushCaptainModule()
    }
    
    // MARK: - -> Interactor -
    // Connection
    func connectionEstablished() {
        view?.setTitle("Updating...")
    }
    
    func dataIsUpToDate() {
        view?.setTitle("Chat list")
    }
    
    func gotCaptainImage(_ img: UIImage?) {
        view?.setCaptainImage(img)
    }
    
    func connectionLost(_ error: WebsocketError) {
        view?.setTitle("Connection...")
    }
    
    // Chat
    func chatWasCreated() {
        view?.setCreateChatInProgress = false
        view?.hideCreateChatPopover()
        router?.isNavbarHidden = false
    }
    
    func chatCreationFailure(_ error: FError) {
        router?.creationChatFailure(error: error)
        view?.setCreateChatInProgress = false
        router?.isNavbarHidden = false
    }
    
    func databaseChangesHadCome() {
        view?.setTitle("Updating...")
        interactor?.orderChatListUpdateRequest()
    }
    
    func chatListWasLoadedFromWarehouse(_ chatList: [Chat]) {
        view?.setTitle("Chat list")
        view?.refreshCollectionView(chatList)
    }
}

//
//  ChatListPresenterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 13/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

protocol ChatListPresenterProtocol: class {
    func setup(view: ChatListCollectionViewControllerProtocol, router: ChatListRouterProtocol, interactor: ChatListInteractorProtocol)
    
    // -> View
    func readyToConnect()
    func createNewChatButtonWasTapped()
    func createChatCancelButtonWasTapped()
    func userWantsToCreateChat(_ name: String)
    func userWantsToSearchPerson()
    func userWantsToSeeCaptainsInfo()
    
    // -> Interactor
    func connectionEstablished()
    func dataIsUpToDate()
    func gotCaptainImage(_ img: UIImage?)
    func connectionLost(_ error: WebsocketError)
    
    func chatWasCreated()
    func chatCreationFailure(_ error: FError)
    
    func databaseChangesHadCome()
    func chatListWasLoadedFromWarehouse(_ chatList: [Chat])
}

//
//  ChatListInteractor.swift
//  Farnsworth
//
//  Created by Marty on 13/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ChatListInteractor {
    // VIPER
    private weak var presenter: ChatListPresenterProtocol?
    
    private let usersPhotoReceiver = UsersPhotoReceiver()
    
    private var updater: ConnectionUpdater?
    private let socket: FarnsworthSocketProtocol = FarnsworthSocket.shared
    
    
    init(presenter: ChatListPresenterProtocol) {
        self.presenter = presenter
        orderLocalCaptainPhotoRequest()
        orderChatListUpdateRequest()
        
        CentralRouter.shared.chatDelegate = self
        addConnectionUpdatedObserver()
        addCaptainsInfoWasUpdatedObserver()
    }
    
    deinit {
        removeConnectionUpdatedObserver()
        removeCaptainsInfoWasUpdatedObserver()
    }
}


extension ChatListInteractor: ChatListInteractorProtocol {
    func readyToConnect() {
        socket.onDisconnect = { [weak self] (_ error: WebsocketError) in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.connectionLost(error)
            }
            
            switch error {
            case .unauthorized:
                print("Unauthorized")
            default:
                print("err")
                self.socket.connect()
            }
        }
        
        socket.onConnect = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.connectionEstablished()
            }
            
            if self.updater == nil {
                self.updater = ConnectionUpdater()
            }
        }
        
        socket.connect()
    }
    
    func createNewChatRequest(title: String) {
        let chatPayload = CreateChatPayload(title: title)
        guard let chatPayloadJSON = try? JSONEncoder().encode(chatPayload),
            let payload = String(data: chatPayloadJSON, encoding: .utf8) else {
            return
        }
        
        let chat = FSOutcomingMessageSchema(messageType: .createChat, payload: payload)
        guard let jsonChat = try? JSONEncoder().encode(chat) else {
            return
        }
        
        if !socket.isConnected {
            presenter?.chatCreationFailure(.err_2_0)
            return
        }
        
        socket.sendMessage(jsonChat) { [weak self] in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.chatWasCreated()
            }
        }
    }
    
    func orderChatListUpdateRequest() {
        CoreDataStackManager.shared.performTaskAsync({ [weak self] (context) in
            guard let self = self else {
                return
            }
            
            let request: NSFetchRequest<FSChat> = FSChat.fetchRequest()
            guard let rawChatList = try? context.fetch(request) else {
                self.chatListFetchingError()
                return
            }
            guard let chatList = self.handleRawChatListData(rawChatList) else {
                self.chatListFetchingError()
                return
            }
            self.chatListWasFetched(chatList)
        }, completion: nil)
    }
    
    func orderLocalCaptainPhotoRequest() {
        CoreDataStackManager.shared.performTaskAsync({ [weak self] (context) in
            guard let self = self else {
                return
            }
            guard let id = Config.shared.userId else {
                return
            }
            
            do {
                guard let rawCap = try FSPerson.findById(id, in: context), let cap = Person.makeFromFSCDEntity(rawCap) else {
                    return
                }
                
                self.sendCaptainsInfoWasUpdatedNotification(cap)
            } catch {
                assertionFailure("ChatListInteractor.orderLocalCaptainPhotoRequest - captain search error -")
            }
            
            
        }, completion: nil)
    }
}


extension ChatListInteractor: WSChatDelegate {
    
    // main Q
    func newChatIsComing(_ chatPayload: NewChatIsComingPayload) {
        
        CoreDataStackManager.shared.performTaskAsync({ [weak self] (context) in
            guard let self = self else {
                return
            }
            self.rewirteChatFromPayload(chatPayload, withContext: context)
        }) {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.databaseChangesHadCome()
            }
        }
    }
    
    func creationChatEndWithError(_ error: CreateChatErrorPayload) {
        print("chat creation error")
    }
}


// Core Data stuff
extension ChatListInteractor {
    private func rewirteChatFromPayload(_ chat: NewChatIsComingPayload, withContext context: NSManagedObjectContext) {
        let newChat = Chat(id: chat.id, version: chat.version)
        
        let author = Person(id: chat.author, version: 0)
        var members = [Person]()
        for memberId in chat.members {
            let member = Person(id: memberId, version: 0)
            members.append(member)
        }
        
        newChat.setInfo(title: chat.title, created: chat.created, author: author, members: members)
        FSChat.rewrite(newChat, in: context)
    }
    
    private func presenterCanUpdateData() {
        // FS.CoreData queue
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            self.presenter?.databaseChangesHadCome()
        }
    }
    
    private func handleRawChatListData(_ rawChasList: [FSChat]) -> [Chat]? {
        // FS.CoreData queue
        var chats = [Chat]()
        
        for rawChat in rawChasList {
            guard let chat = Chat.makeFromFSCDEntity(rawChat) else {
                return nil
            }
            chats.append(chat)
        }
        
        return chats
    }
    
    private func chatListWasFetched(_ chatList: [Chat]) {
        // FS.CoreData queue
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            
            self.presenter?.chatListWasLoadedFromWarehouse(chatList)
        }
    }
    
    private func chatListFetchingError() {
        // FS.CoreData queue
        print("chat list fetching error")
    }
}


// MARK: - Observers -
extension ChatListInteractor {
    private func sendCaptainsInfoWasUpdatedNotification(_ captain: Person) {
        NotificationCenter.default.post(name: Notification.Name.captainsInfoWasUpdated, object: nil, userInfo: ["info": captain])
    }
    
    private func addConnectionUpdatedObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(connectionUpdaterSuccessUpdate), name: Notification.Name.connectionUpdaterSuccessUpdate, object: nil)
    }
    
    private func removeConnectionUpdatedObserver() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.connectionUpdaterSuccessUpdate, object: nil)
    }
    
    @objc private func connectionUpdaterSuccessUpdate() {
        updater = nil
        orderChatListUpdateRequest()
    }
    
    
    private func addCaptainsInfoWasUpdatedObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(captainsInfoWasUpdated), name: Notification.Name.captainsInfoWasUpdated, object: nil)
    }
    
    private func removeCaptainsInfoWasUpdatedObserver() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.captainsInfoWasUpdated, object: nil)
    }
    
    @objc private func captainsInfoWasUpdated(notification: Notification) {
        guard let payload = notification.userInfo?["info"] as? Person? else {
            return
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            self.presenter?.gotCaptainImage(payload?.photo)
        }
    }
}


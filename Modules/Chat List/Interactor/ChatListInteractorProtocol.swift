//
//  ChatListInteractorProtocol.swift
//  Farnsworth
//
//  Created by Marty on 13/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol ChatListInteractorProtocol: class {
    // View ->
    func readyToConnect()
    func createNewChatRequest(title: String)
    func orderChatListUpdateRequest()
    func orderLocalCaptainPhotoRequest()
}

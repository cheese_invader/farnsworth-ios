//
//  ChatListCollectionViewControllerProtocol.swift
//  Farnsworth
//
//  Created by Marty on 13/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

protocol ChatListCollectionViewControllerProtocol: class {
    func setup(presenter: ChatListPresenterProtocol)
    
    func refreshCollectionView(_ chatList: [Chat])
    
    func setTitle(_ title: String)
    func setCaptainImage(_ img: UIImage?)
    
    func showCreateChatPopup()
    func hideCreateChatPopover()
    func shakeCreateChatPopoverTextField()
    var setCreateChatInProgress: Bool { get set }
}

//
//  ChatListCollectionViewController.swift
//  Farnsworth
//
//  Created by Marty on 13/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit
import Foundation
import Starscream


private let popoverTransformIndex: CGFloat = 1.3
private let popoverDuration = 0.4
private let captainImageSize: CGFloat = 36
private let captainImageOffset: CGFloat = 10


private let reuseIdentifier = "ChatListCell"
private let cellNibName     = "ChatListCollectionViewCell"


class ChatListCollectionViewController: UICollectionViewController {
    // VIPER
    private var presenter: ChatListPresenterProtocol?
    
    
    // MARK: - Property -
    private var chatList = [Chat]()
    private let captainButton = UIButton()
    private var blurEffect: UIVisualEffect?
    private var blurVisualEffectView: UIVisualEffectView!
    
    
    // MARK: - Outlets -
//    @IBOutlet weak var blurVisualEffectView: UIVisualEffectView!
    @IBOutlet var createChatPopover: UIView!
    @IBOutlet weak var createChatActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var createChatCreateButton: RoundButton!
    @IBOutlet weak var createChatNameTextField: ShakeTextField!
    
    
    // MARK: - Actions -
    @IBAction func addChatButtonWasTapped(_ sender: UIBarButtonItem) {
        presenter?.createNewChatButtonWasTapped()
    }
    
    @IBAction func createChatCancelButtonWasTapped(_ sender: UIButton) {
        presenter?.createChatCancelButtonWasTapped()
    }
    @IBAction func createChatCreateButtonWasTapped(_ sender: UIButton) {
        presenter?.userWantsToCreateChat(createChatNameTextField.text ?? "")
    }
    
    @IBAction func createChatNameTextFieldPrimaryActionDidTriggered(_ sender: ShakeTextField) {
        presenter?.userWantsToCreateChat(createChatNameTextField.text ?? "")
    }
    
    @IBAction func searchBarButtonWasTapped(_ sender: UIBarButtonItem) {
        presenter?.userWantsToSearchPerson()
    }
    
    @IBAction func captainButtonWasTapped(_ sender: UIButton) {
        presenter?.userWantsToSeeCaptainsInfo()
    }
    
    
    // Collection VC stuff
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCaptainImage()
        
        hideKeyboardWhenTappedAround()
        addKeyboardObservers()
        
        setupBlur()
        registerNib()
        
        presenter?.readyToConnect()
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeKeyboardObservers()
        
        showImage(false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showImage(true)
    }
}


extension ChatListCollectionViewController: ChatListCollectionViewControllerProtocol {
    func setup(presenter: ChatListPresenterProtocol) {
        self.presenter = presenter
    }
    
    func refreshCollectionView(_ chatList: [Chat]) {
        self.chatList = chatList
        self.collectionView.reloadData()
    }
    
    func setTitle(_ title: String) {
        navigationItem.title = title
    }
    
    func setCaptainImage(_ img: UIImage?) {
        captainButton.setImage(img ?? UIImage(named: "Empty_user_photo"), for: .normal)
    }
    
    func showCreateChatPopup() {
        self.view.addSubview(createChatPopover)
        createChatPopover.center = self.view.center
        createChatPopover.transform = CGAffineTransform.init(scaleX: popoverTransformIndex, y: popoverTransformIndex)
        
        UIView.animate(withDuration: popoverDuration) { [weak self] in
            guard let self = self else {
                return
            }
            self.blurVisualEffectView.isUserInteractionEnabled = true
            self.blurVisualEffectView.effect = self.blurEffect
            self.createChatPopover.alpha = 1
            self.createChatPopover.transform = CGAffineTransform.identity
        }
    }
    
    func hideCreateChatPopover() {
        UIView.animate(withDuration: popoverDuration, animations: { [weak self] in
            guard let self = self else {
                return
            }
            self.blurVisualEffectView.isUserInteractionEnabled = false
            self.createChatPopover.transform = CGAffineTransform.init(scaleX:  2 - popoverTransformIndex, y: 2 - popoverTransformIndex)
            self.createChatPopover.alpha = 0
            self.blurVisualEffectView.effect = nil
            
        }) { [weak self] (_) in
            guard let self = self else {
                return
            }
            self.createChatPopover.removeFromSuperview()
            self.createChatNameTextField.text = ""
        }
    }
    
    func shakeCreateChatPopoverTextField() {
        createChatNameTextField.shake()
    }
    
    var setCreateChatInProgress: Bool {
        get {
            return createChatActivityIndicator.isAnimating
        }
        set {
            if newValue {
                createChatActivityIndicator.startAnimating()
            } else {
                createChatActivityIndicator.stopAnimating()
            }
            createChatNameTextField.isEnabled = !newValue
            createChatCreateButton .isEnabled = !newValue
        }
    }
}


// MARK: - UICollectionViewDelegateFlowLayout -
extension ChatListCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth  = view.frame.size.width - 20
        let cellHeight: CGFloat = cellWidth / 3

        return CGSize(width: cellWidth, height: cellHeight)
    }
}


// MARK: - UICollectionViewDataSource -
extension ChatListCollectionViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chatList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ChatListCollectionViewCell
        
        cell.assembleFromChat(chatList[indexPath.row])
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}


// MARK: - Navbar image -
extension ChatListCollectionViewController {
    private func showImage(_ show: Bool) {
        UIView.animate(withDuration: 0.4) { [weak self] in
            guard let self = self else {
                return
            }
            self.captainButton.alpha = show ? 1.0 : 0.0
        }
    }
    
    // Magic to set button near large title in navigation bar
    private func setupCaptainImage() {
        guard
            let navbar = self.navigationController?.navigationBar,
            navbar.subviews.count > 1
        else {
            return
        }
        captainButton.addTarget(self, action: #selector(captainButtonWasTapped), for: .touchUpInside)
        navbar.subviews[1].insertSubview(captainButton, at: 0)
        
        captainButton.layer.cornerRadius = captainImageSize / 2
        captainButton.clipsToBounds = true
        captainButton.imageView?.contentMode = .scaleAspectFill
        captainButton.contentMode = .scaleAspectFill
        captainButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            captainButton.rightAnchor .constraint(equalTo:  navbar.rightAnchor,  constant: -captainImageOffset),
            captainButton.bottomAnchor.constraint(equalTo: navbar.bottomAnchor, constant: -captainImageOffset),
            captainButton.heightAnchor.constraint(equalToConstant: captainImageSize),
            captainButton.widthAnchor .constraint(equalTo: captainButton.heightAnchor)
        ])
    }
}


// MARK: - Helpers -
extension ChatListCollectionViewController {
    func setupBlur() {
        blurEffect = UIBlurEffect(style: .dark)
        blurVisualEffectView = UIVisualEffectView(effect: nil)
        blurVisualEffectView.isUserInteractionEnabled = false
        collectionView.addSubview(blurVisualEffectView)
        
        blurVisualEffectView.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint    = blurVisualEffectView.topAnchor   .constraint(equalTo: view.topAnchor)
        let leftConstraint   = blurVisualEffectView.leftAnchor  .constraint(equalTo: view.leftAnchor)
        let rightConstraint  = blurVisualEffectView.rightAnchor .constraint(equalTo: view.rightAnchor)
        let bottomConstraint = blurVisualEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        
        view.addConstraints([topConstraint, leftConstraint, rightConstraint, bottomConstraint])
    }
    
    func registerNib() {
        let nibCell = UINib(nibName: cellNibName, bundle: nil)
        self.collectionView!.register(nibCell, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: self.view.window)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                let currentPosition = self.view.center.y
                let needToBe = (self.view.frame.height - keyboardSize.height) / 2
                
                let offset = currentPosition - needToBe
                self.view.frame.origin.y -= offset
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
}

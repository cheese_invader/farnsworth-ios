//
//  ChatListCollectionViewCell.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class ChatListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var title        : UILabel!
    @IBOutlet weak var message      : UILabel!
    @IBOutlet weak var chatStatus   : UILabel!
    @IBOutlet weak var messageStatus: UILabel!
    
    @IBOutlet weak var lastMessageTime: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var imageLoading: UIActivityIndicatorView!
    
    func assembleFromChat(_ chat: Chat) {
        if chat.completeness != .high {
            imageLoading.startAnimating()
        }
        
        if chat.completeness == .low {
            return
        }
        
        title.text = chat.title
    }
}

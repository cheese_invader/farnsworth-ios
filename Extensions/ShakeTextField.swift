//
//  ShakeTextField.swift
//  Farnsworth
//
//  Created by Marty on 11/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

class ShakeTextField: UITextField {

    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5, y: self.center.y))
        animation.toValue   = NSValue(cgPoint: CGPoint(x: self.center.x + 5, y: self.center.y))
        
        self.layer.add(animation, forKey: "position")
    }

}

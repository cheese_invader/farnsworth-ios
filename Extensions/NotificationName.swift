//
//  NotificationName.swift
//  Farnsworth
//
//  Created by Marty on 29/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let connectionUpdaterSuccessUpdate = Notification.Name("FS.ConnectionUpdater.success")
    static let gotChatsDetailInfo             = Notification.Name("FS.CentralRouter.gotChatsDetailInfo")
    static let gotYourPrivateData             = Notification.Name("FS.CentralRouter.gotYourPrivateData")
    static let gotUsersPhoto                  = Notification.Name("FS.CentralRouter.gotUsersPhoto")
    static let gotUsersInfo                   = Notification.Name("FS.CentralRouter.gotUsersInfo")
    static let captainsInfoWasUpdated         = Notification.Name("FS.UsersPhotoReceiver.captainInfoWasUpdated")
}

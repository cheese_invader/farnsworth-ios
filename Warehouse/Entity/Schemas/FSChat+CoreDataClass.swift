//
//  FSChat+CoreDataClass.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//
//

import Foundation
import CoreData


public class FSChat: NSManagedObject {
    func composeWithEntity(_ entity: Chat, in context: NSManagedObjectContext) throws {
        self.id           = entity.id
        self.version      = Int64(entity.version)
        self.completeness = entity.completeness.rawValue
        
        if entity.completeness == .low {
            return
        }
        self.title   = entity.title
        self.created = entity.created
        
        do {
            let author = try FSPerson.findOrCreate(entity.author, in: context)
            self.author = author
            
            
            for member in entity.members {
                let rawMember = try FSPerson.findOrCreate(member, in: context)
                self.addToMembers(rawMember)
            }
        } catch {
            throw error
        }

        self.photo = entity.photo?.pngData()
    }
    
    class func findOrCreateChat(matching chat: Chat, in context: NSManagedObjectContext) throws -> FSChat {
        let request: NSFetchRequest<FSChat> = FSChat.fetchRequest()
        request.predicate = NSPredicate(format: "id = %@", chat.id)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "FSChat.findOrCreateChat -- database inconsistency")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        let newChat = FSChat(context: context)
        do {
            try newChat.composeWithEntity(chat, in: context)
        } catch {
            throw error
        }
        
        return newChat
    }
    
    class func findById(_ id: String, in context: NSManagedObjectContext) throws -> FSChat? {
        let requst: NSFetchRequest<FSChat> = FSChat.fetchRequest()
        requst.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let matches = try context.fetch(requst)
            if matches.count > 0 {
                assert(matches.count == 1, "FSChat.findById -- database inconsistency")
                return matches[0]
            } else {
                return nil
            }
        } catch {
            throw error
        }
    }
    
    class func rewrite(_ chat: Chat, in context: NSManagedObjectContext) {
        do {
            let oldChat = try FSChat.findById(chat.id, in: context)
            
            guard let newChat = oldChat else {
                _ = try FSChat.findOrCreateChat(matching: chat, in: context)
                return
            }
            
            newChat.version      = Int64(chat.version)
            newChat.completeness = chat.completeness.rawValue
            
            if chat.completeness == .low {
                return
            }
            newChat.title   = chat.title
            newChat.created = chat.created
            newChat.author  = try FSPerson.findOrCreate(chat.author, in: context)
            
            for member in chat.members {
                newChat.addToMembers(try FSPerson.findOrCreate(member, in: context))
            }
            
            if chat.completeness == .middle {
                return
            }
            newChat.photo = chat.photo?.pngData()
        } catch {
            return
        }
    }
}

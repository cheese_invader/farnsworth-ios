//
//  FSCaptain+CoreDataClass.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//
//

import Foundation
import CoreData


public class FSCaptain: NSManagedObject {
    class func getCaptain(in context: NSManagedObjectContext) throws -> FSCaptain? {
        let request: NSFetchRequest<FSCaptain> = FSCaptain.fetchRequest()
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "FSCaptain.findOrCreateCaptain -- database inconsistency")
                return matches[0]
            }
        } catch {
            throw error
        }
        return nil
    }
    
    class func rewriteCaptain(_ entity: Captain, in context: NSManagedObjectContext) {
        do {
            if let oldCaptain = try FSCaptain.getCaptain(in: context) {
                context.delete(oldCaptain)
            }
            let cap = FSCaptain(context: context)
            try cap.composeWithEntity(entity, in: context)
        } catch {
            return
        }
    }
    
    func composeWithEntity(_ entity: Captain, in context: NSManagedObjectContext) throws {
        do {
            let rawMe = try FSPerson.findOrCreate(entity.me, in: context)
            self.me = rawMe
            
            for person in entity.contacts {
                let rawPerson = try FSPerson.findOrCreate(person, in: context)
                self.addToContacts(rawPerson)
            }
            
            for person in entity.blackList {
                let rawPerson = try FSPerson.findOrCreate(person, in: context)
                self.addToBlackList(rawPerson)
            }
        } catch {
            throw error
        }
    }
}

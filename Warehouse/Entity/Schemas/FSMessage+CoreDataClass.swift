//
//  FSMessage+CoreDataClass.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//
//

import Foundation
import CoreData


public class FSMessage: NSManagedObject {
    func composeWithEntity(_ entity: Message) {
        self.id = entity.id
        
//        let author = FSPerson()
//        author.composeWithEntity(entity.author)
//
//        let chat = FSChat()
//        chat.composeWithEntity(entity.chat)
//
//        self.author = author
//        self.chat   = chat
        
        self.text = entity.text
        self.data = entity.data
        if let imgs = entity.imgs {
            self.imgs = ImageArrayCoder().encodeImageArray(imgs)
        }
        
    }
}

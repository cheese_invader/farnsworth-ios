//
//  FSPerson+CoreDataClass.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//
//

import Foundation
import CoreData


public class FSPerson: NSManagedObject {
    func composeWithEntity(_ entity: Person) {
        self.id            = entity.id
        self.version       = Int64(entity.version)
        self.completeness  = entity.completeness.rawValue
        
        if entity.completeness == .low {
            return
        }
        self.firstName     = entity.firstName
        self.lastName      = entity.lastName
        self.gender        = entity.gender.rawValue
        self.status        = entity.status.rawValue
        self.lastSeen      = entity.lastSeen
        self.username      = entity.username
        self.bio           = entity.bio
        
        if entity.completeness == .hihg {
            // Perfectionism
            self.photo = entity.photo?.pngData()
        }
    }
    
    class func findOrCreate(_ person: Person, in context: NSManagedObjectContext) throws -> FSPerson {
        let request: NSFetchRequest<FSPerson> = FSPerson.fetchRequest()
        request.predicate = NSPredicate(format: "id = %@", person.id)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "FSPerson.findOrCreatePerson -- database inconsistency")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        let newPerson = FSPerson(context: context)
        newPerson.composeWithEntity(person)
        return newPerson
    }
    
    class func findById(_ id: String, in context: NSManagedObjectContext) throws -> FSPerson? {
        let requst: NSFetchRequest<FSPerson> = FSPerson.fetchRequest()
        requst.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let matches = try context.fetch(requst)
            if matches.count > 0 {
                assert(matches.count == 1, "FSPerson.findOrCreatePerson -- database inconsistency")
                return matches[0]
            } else {
                return nil
            }
        } catch {
            throw error
        }
    }
    
    class func rewrite(_ person: Person, in context: NSManagedObjectContext) {
        do {
            let oldUser = try FSPerson.findById(person.id, in: context)
            
            guard let user = oldUser else {
                _ = try FSPerson.findOrCreate(person, in: context)
                return
            }
            
            user.version      = Int64(person.version)
            user.completeness = person.completeness.rawValue
            if person.completeness == .low {
                return
            }
            
            user.firstName    = person.firstName
            user.lastName     = person.lastName
            user.gender       = person.gender.rawValue
            user.status       = person.status.rawValue
            user.lastSeen     = person.lastSeen
            user.username     = person.username
            user.bio          = person.bio
            
            if person.completeness == .middle {
                return
            }
            user.photo = person.photo?.pngData()
            
            return
        } catch {
            return
        }
    }
}

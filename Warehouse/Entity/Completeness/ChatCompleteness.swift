//
//  ChatCompleteness.swift
//  Farnsworth
//
//  Created by Marty on 08/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

/*
 
 low: chat has id, version, completeness = low
 
 middle: chat has low + title, created, author, members
 
 high: chat has middle + photo
 
 */

enum ChatCompleteness: String {
    case low
    case middle
    case high
}

//
//  UserCompleteness.swift
//  Farnsworth
//
//  Created by Marty on 08/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

/*
 
 low: user has id, version, completeness = low
 
 middle: user has low + firstName, lastName, gender, status, lastSeen, username?, bio?
 
 high: user has middle + photo
 
*/


enum UserCompleteness: String {
    case low
    case middle
    case hihg
}

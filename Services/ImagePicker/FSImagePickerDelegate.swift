//
//  FSImagePickerDelegate.swift
//  Farnsworth
//
//  Created by Marty on 21/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

protocol FSImagePickerDelegate: class {
    func gotImage(_ image: UIImage)
}

//
//  FSImagePicker.swift
//  Farnsworth
//
//  Created by Marty on 21/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit


class FSImagePicker: UINavigationController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    weak var navigation: NavigationViewController?
    weak var fsDelegate: FSImagePickerDelegate?
    
    private var source = UIImagePickerController.SourceType.camera
}


extension FSImagePicker: FSImagePickerProtocol {
    func setNavigation(_ navigation: NavigationViewController?) {
        self.navigation = navigation
    }
    
    func setDelegate(_ delegate: FSImagePickerDelegate?) {
        self.fsDelegate = delegate
    }
    
    func takeImage() {
        takeImageFrom()
    }
    
    func setSource(_ source: UIImagePickerController.SourceType) {
        self.source = source
    }
    
    private func takeImageFrom() {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = source
        image.allowsEditing = false
        navigation?.present(image, animated: true, completion: nil)
    }
    
    private func rotateImage(image: UIImage) -> UIImage {
        if (image.imageOrientation == .up) {
            return image
        }
        
        UIGraphicsBeginImageContext(image.size)
        
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return copy!
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            fsDelegate?.gotImage(rotateImage(image: image))
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

//
//  FSImagePickerProtocol.swift
//  Farnsworth
//
//  Created by Marty on 21/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit

protocol FSImagePickerProtocol {
    func setNavigation(_ navigation: NavigationViewController?)
    func setDelegate(_ delegate: FSImagePickerDelegate?)
    func takeImage()
    func setSource(_ source: UIImagePickerController.SourceType)
}

//
//  ConnectionUpdater.swift
//  Farnsworth
//
//  Created by Marty on 29/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import UIKit
import CoreData

// gotPrivateData -> (gotChatsDetailInfo & gotUsersInfo) -> Done

class ConnectionUpdater {
    private let socket: FarnsworthSocketProtocol = FarnsworthSocket.shared
    
    private var areChatsUpdated = false {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                if self.areUsersUpdated {
                    self.sendUpdateCompleteNotification()
                }
            }
        }
    }
    private var areUsersUpdated = false {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                if self.areChatsUpdated {
                    self.sendUpdateCompleteNotification()
                }
            }
        }
    }
    
    init() {
        addGotChatDetailInfoObserver()
        addGotYourPrivateDataObserver()
        addGotUsersInfoObserver()
    }
    
    deinit {
        removeGotChatDetailInfoObserver()
        removeGotYourPrivateDataObserver()
        removeGotUsersInfoObserver()
    }
    
    func gotPrivateData(_ data: YourProvateDataPayload) {
        CoreDataStackManager.shared.performTaskAsync({ [weak self] (context) in
            guard let self = self else {
                return
            }
            
            self.syncChatList            (privateData: data, in: context)
            self.syncContactsAndBlackList(privateData: data, in: context)
            
            do {
                try context.save()
            } catch {
                assertionFailure("ConnectionUpdater.gotPrivateData - saving context probelm -")
            }
        }, completion: nil)
    }
    
    func gotChatsDetailInfo(_ info: ChatsDetailInfoPayload) {
        CoreDataStackManager.shared.performTaskAsync({ [weak self] (context) in
            guard let _ = self else {
                return
            }
            for chat in info.chats {
                let newChat = Chat(id: chat.id, version: chat.version)
                
                let author = Person(id: chat.author, version: 0)
                var members = [Person]()
                for memberId in chat.members {
                    let member = Person(id: memberId, version: 0)
                    members.append(member)
                }
                
                newChat.setInfo(title: chat.title, created: chat.created, author: author, members: members)
                FSChat.rewrite(newChat, in: context)
            }
            do {
                try context.save()
            } catch {
                assertionFailure("ConnectionUpdater.gotChatsDetailInfo - context saving error -")
            }
        }, completion: { [weak self] in
            guard let self = self else {
                return
            }
            
            // TODO: update chats photos
            
            self.areChatsUpdated = true
        })
    }
    
    func gotUsersInfo(_ info: UsersInfoPayload) {
        CoreDataStackManager.shared.performTaskAsync({ [weak self] (context) in
            guard let _ = self else {
                return
            }
            
            for user in info.info {
                let userEntity = Person(id: user.id, version: user.version)
                userEntity.setInfo(firstName: user.firstName, lastName: user.lastName, gender: user.gender, status: user.status.status, lastSeen: user.status.lastSeen, username: user.username, bio: user.bio)
                FSPerson.rewrite(userEntity, in: context)
            }
            
            do {
                try context.save()
            } catch {
                assertionFailure("ConnectionUpdater.gotUsersInfo - context saving error -")
                return
            }
        }, completion: { [weak self] in
            guard let self = self else {
                return
            }
            
            var toUpdatePhoto = [String]()
            for user in info.info {
                toUpdatePhoto.append(user.id)
            }
            
            if !toUpdatePhoto.isEmpty {
                self.sendGetUserPhotosRequest(toUpdatePhoto)
            }
            
            self.areUsersUpdated = true
        })
    }
}


// MARK: - Helpers -
extension ConnectionUpdater {
    
    // MARK: - deletion -
    private func deleteOldChats(_ chatList: [CompressedEntityPayload], in context: NSManagedObjectContext) {
        // FS.CoreData queue
        var newChatsIds = Set<String>()
        var oldChatsIds = Set<String>()
        
        let request: NSFetchRequest<FSChat> = FSChat.fetchRequest()
        let oldChats = (try? context.fetch(request)) ?? []
        
        for newChat in chatList {
            newChatsIds.insert(newChat.i)
        }
        
        for oldChat in oldChats {
            oldChatsIds.insert(oldChat.id!)
        }
        
        let toDeleteIds = oldChatsIds.subtracting(newChatsIds)
        
        for chat in oldChats where toDeleteIds.contains(chat.id!) {
            context.delete(chat)
        }
    }
    
    private func deleteOldUsers(_ users: [CompressedEntityPayload], in context: NSManagedObjectContext) {
        // FS.CoreData queue
        var newUserIds = Set<String>()
        var oldUserIds = Set<String>()
        
        let request: NSFetchRequest<FSPerson> = FSPerson.fetchRequest()
        let oldUsers = (try? context.fetch(request)) ?? []
        
        for newUser in users {
            newUserIds.insert(newUser.i)
        }
        
        for oldUser in oldUsers {
            oldUserIds.insert(oldUser.id!)
        }
        
        let toDeleteIds = oldUserIds.subtracting(newUserIds)
        
        for user in oldUsers where toDeleteIds.contains(user.id!) {
            context.delete(user)
        }
    }
    
    // MARK: - refreshing -
    private func refreshChats(_ chatList: [FSChat]) {
        var toUpdate = [String]()
        
        for rawChat in chatList {
            toUpdate.append(rawChat.id!)
        }
        
        if !toUpdate.isEmpty {
            sendRefreshChatsRequest(ids: toUpdate)
        } else {
            areChatsUpdated = true
        }
    }
    
    private func refreshUsers(_ users: [FSPerson]) {
        var toUpdate = [String]()
        
        for rawUser in users {
            toUpdate.append(rawUser.id!)
        }
        
        if !toUpdate.isEmpty {
            sendGetUsersInfoRequest(toUpdate)
        } else {
            areUsersUpdated = true
        }
    }
    
    
    // MARK: - refreshing request -
    private func sendRefreshChatsRequest(ids: [String]) {
        let chatInfoPayload = NeedChatsInfoPayload(ids: ids)
        guard
            let chatPayloadJSON = try? JSONEncoder().encode(chatInfoPayload),
            let payload = String(data: chatPayloadJSON, encoding: .utf8)
        else {
            return
        }
        
        let chatInfo = FSOutcomingMessageSchema(messageType: .needChatsInfo, payload: payload)
        guard let jsonChatInfo = try? JSONEncoder().encode(chatInfo) else {
            return
        }
        if !socket.isConnected {
            return
        }
        
        socket.sendMessage(jsonChatInfo, nil)
    }
    
    private func sendGetUsersInfoRequest(_ ids: [String]) {
        let getUsersPayload = GetUsersInfoPayload(ids: ids)
        guard
            let getUsersPayloadJson = try? JSONEncoder().encode(getUsersPayload),
            let payload = String(data: getUsersPayloadJson, encoding: .utf8)
        else {
            return
        }
        
        let getUsersInfo = FSOutcomingMessageSchema(messageType: .getUsersInfo, payload: payload)
        guard let getUsersInfoJson = try? JSONEncoder().encode(getUsersInfo) else {
            return
        }
        
        if !socket.isConnected {
            return
        }
        
        socket.sendMessage(getUsersInfoJson, nil)
    }
    
    private func sendGetUserPhotosRequest(_ ids: [String]) {
        let getMyPhotoPayload = GetUserPhotosPayload(ids: ids)
        guard
            let getMyPhotoPayloadJson = try? JSONEncoder().encode(getMyPhotoPayload),
            let payload = String(data: getMyPhotoPayloadJson, encoding: .utf8)
        else {
            return
        }
        
        let getMyPhotoInfo = FSOutcomingMessageSchema(messageType: .getUsersPhotos, payload: payload)
        guard let getMyPhotoJSONInfo = try? JSONEncoder().encode(getMyPhotoInfo) else {
            return
        }
        if !socket.isConnected {
            return
        }
        
        socket.sendMessage(getMyPhotoJSONInfo, nil)
    }
    
    // MARK: - sync -
    private func syncChatList(privateData: YourProvateDataPayload, in context: NSManagedObjectContext) {
        var chatsNeedToRefresh = [FSChat]()
        self.deleteOldChats(privateData.chatList, in: context)
        
        for chatInfo in privateData.chatList {
            let chat = Chat(id: chatInfo.i, version: chatInfo.v)
            
            guard
                let rawChat = try? FSChat.findOrCreateChat(matching: chat, in: context),
                let rawCompleteness = rawChat.completeness,
                let completeness = ChatCompleteness.init(rawValue: rawCompleteness)
            else {
                continue
            }
            
            if completeness != .high || Int(rawChat.version) != chat.version {
                chatsNeedToRefresh.append(rawChat)
            }
        }
        
        self.refreshChats(chatsNeedToRefresh)
    }
    
    private func syncContactsAndBlackList(privateData: YourProvateDataPayload, in context: NSManagedObjectContext) {
        var userNeedToRefresh = [FSPerson]()
        let users = privateData.contacts + privateData.blackList + [CompressedEntityPayload(i: privateData.id, v: privateData.version)]
        deleteOldUsers(users, in: context)
        
        
        guard let me = try? FSPerson.findOrCreate(Person(id: privateData.id, version: privateData.version), in: context) else {
            assertionFailure("ConnectionUpdater.syncContactsAndBlackList - can't create captain -")
            return
        }
        
        if Int(me.version) != privateData.version || UserCompleteness(rawValue: me.completeness!)! != .hihg {
            userNeedToRefresh.append(me)
        }
        
        for contact in privateData.contacts {
            guard let person = try? FSPerson.findOrCreate(Person(id: contact.i, version: contact.v), in: context) else {
                assertionFailure("ConnectionUpdater.syncContactsAndBlackList - can't create contact -")
                continue
            }
            if Int(person.version) != contact.v ||  UserCompleteness(rawValue: person.completeness!)! != .hihg {
                userNeedToRefresh.append(person)
            }
        }
        for contact in privateData.blackList {
            guard let person = try? FSPerson.findOrCreate(Person(id: contact.i, version: contact.v), in: context) else {
                assertionFailure("ConnectionUpdater.syncContactsAndBlackList - can't create black list user -")
                continue
            }
            if Int(person.version) != contact.v ||  UserCompleteness(rawValue: person.completeness!)! != .hihg {
                userNeedToRefresh.append(person)
            }
        }
        
        refreshUsers(userNeedToRefresh)
    }
}


// MARK: - Observers -
extension ConnectionUpdater {
    // Send
    private func sendUpdateCompleteNotification() {
        NotificationCenter.default.post(name: Notification.Name.connectionUpdaterSuccessUpdate, object: nil)
    }
    
    // Get
    private func addGotYourPrivateDataObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(gotYourPrivateDataNotification), name: Notification.Name.gotYourPrivateData, object: nil)
    }
    
    private func addGotChatDetailInfoObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(gotChatDetailInfoNotification), name: Notification.Name.gotChatsDetailInfo, object: nil)
    }
    
    private func addGotUsersInfoObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(gotUsersInfoNotification), name: Notification.Name.gotUsersInfo, object: nil)
    }
    
    
    // Remove
    private func removeGotYourPrivateDataObserver() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.gotYourPrivateData, object: nil)
    }
    
    private func removeGotChatDetailInfoObserver() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.gotChatsDetailInfo, object: nil)
    }
    
    private func removeGotUsersInfoObserver() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.gotUsersInfo, object: nil)
    }
    
    
    @objc private func gotChatDetailInfoNotification(notification: Notification) {
        guard let payload = notification.userInfo?["info"] as? ChatsDetailInfoPayload else {
            return
        }
        self.gotChatsDetailInfo(payload)
    }
    
    @objc private func gotYourPrivateDataNotification(notification: Notification) {
        guard let payload = notification.userInfo?["info"] as? YourProvateDataPayload else {
            return
        }
        self.gotPrivateData(payload)
    }
    
    @objc private func gotUsersInfoNotification(notification: Notification) {
        guard let payload = notification.userInfo?["info"] as? UsersInfoPayload else {
            return
        }
        self.gotUsersInfo(payload)
    }
}

//
//  FarnsworthSocket.swift
//  Farnsworth
//
//  Created by Marty on 02/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import Starscream

class FarnsworthSocket {
    static let shared = FarnsworthSocket()
    private init() {}
    
    
    deinit {
        disconnect()
    }
    
    private var socket: WebSocket?
    
    private var onConnection: (() -> Void)?
    private var onDisconnection: ((_ error: WebsocketError) -> Void)?
}


extension FarnsworthSocket: WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        onConnection?()
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        guard let error = error else {
            onDisconnection?(.drop)
            return
        }
        onDisconnection?(WebsocketError.defineFromError(error))
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        CentralReceiver.shared.messageIsComing(text)
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("get data")
    }
}


extension FarnsworthSocket: FarnsworthSocketProtocol {
    var isConnected: Bool {
        return socket?.isConnected ?? false
    }
    
    var onConnect: (() -> Void)? {
        get {
            return onConnection
        }
        set {
            onConnection = newValue
        }
    }
    
    var onDisconnect: ((WebsocketError) -> Void)? {
        get {
            return onDisconnection
        }
        set {
            onDisconnection = newValue
        }
    }
    
    func connect() {
        var request = URLRequest(url: Static.shared.baseWebsocketURL)
        request.timeoutInterval = 5
        request.setValue(Config.shared.accessToken, forHTTPHeaderField: "fstoken")
        socket = WebSocket(request: request)
        socket?.delegate = self
        socket?.connect()
    }
    
    func disconnect() {
        socket?.disconnect(forceTimeout: 0)
    }
    
    func sendMessage(_ message: String, _ completion: (() -> Void)?) {
        socket?.write(string: message) {
            completion?()
        }
    }
    
    func sendMessage(_ message: Data, _ completion: (() -> Void)?) {
        socket?.write(data: message, completion: {
            completion?()
        })
    }
}

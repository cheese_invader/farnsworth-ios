//
//  FarnsworthSocketProtocol.swift
//  Farnsworth
//
//  Created by Marty on 02/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import Starscream

protocol FarnsworthSocketProtocol: class {
    var isConnected: Bool { get }
        
    var onConnect   : (() -> Void)? { get set }
    var onDisconnect: ((_ error: WebsocketError) -> Void)? { get set }
    
    func connect()
    func disconnect()
    func sendMessage(_ message: String, _ completion: (() -> Void)?)
    func sendMessage(_ message: Data,   _ completion: (() -> Void)?)
}

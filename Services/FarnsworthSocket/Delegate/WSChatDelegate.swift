//
//  WSChatDelegate.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol WSChatDelegate: class {
    func newChatIsComing(_ chatPayload: NewChatIsComingPayload)
    func creationChatEndWithError(_ error: CreateChatErrorPayload)
}

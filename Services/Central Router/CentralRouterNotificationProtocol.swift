//
//  CentralRouterNotificationProtocol.swift
//  Farnsworth
//
//  Created by Marty on 30/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol CentralRouterNotificationProtocol {
    func sendGotChatsDetailInfoNotification(info: ChatsDetailInfoPayload)
    func sendGotYourPrivateDataNotification(info: YourProvateDataPayload)
    func sendGotUsersPhotoNotification(info: UsersPhotoPayload)
}

//
//  CentralRouter.swift
//  Farnsworth
//
//  Created by Marty on 12/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

class CentralRouter {
    static let shared = CentralRouter()
    private init() {}
    
    weak var chatDelegate: WSChatDelegate?
    weak var searchUsersDelegate: WSSearchUsersDelegate?
}


extension CentralRouter: CentralRouterProtocol {
    func routePayload(_ payload: String, ofType type: IncomingMessageType) {
        switch type {
        case .createChatError:
            guard let error = FarnsworthMessageDecoder().decodeCreateChatError(payload) else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.chatDelegate?.creationChatEndWithError(error)
            }
        case .newChatIsComing:
            guard let chat = FarnsworthMessageDecoder().decodeNewChatIsComingPayload(payload) else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.chatDelegate?.newChatIsComing(chat)
            }
        case .yourPrivateData:
            guard let privatData = FarnsworthMessageDecoder().decodeYourPrivateData(payload) else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.sendGotYourPrivateDataNotification(info: privatData)
            }
        case .chatsDetailInfo:
            guard let detailInfo = FarnsworthMessageDecoder().decodeChatsDetailInfoPayload(payload) else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.sendGotChatsDetailInfoNotification(info: detailInfo)
            }
        case .foundUsers:
            guard let foundUsers = FarnsworthMessageDecoder().decodeFoundUsersPayload(payload) else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.searchUsersDelegate?.usersWereFound(foundUsers)
            }
        case .usersPhoto:
            guard let info = FarnsworthMessageDecoder().decodeUsersPhotoPayload(payload) else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.sendGotUsersPhotoNotification(info: info)
            }
        case .usersInfo:
            guard let info = FarnsworthMessageDecoder().decodeUsersInfoPayload(payload) else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.sendGotUsersInfoNotification(info: info)
            }
        }
    }
}


extension CentralRouter: CentralRouterNotificationProtocol {
    func sendGotChatsDetailInfoNotification(info: ChatsDetailInfoPayload) {
        NotificationCenter.default.post(name: Notification.Name.gotChatsDetailInfo, object: nil, userInfo: ["info": info])
    }
    
    func sendGotYourPrivateDataNotification(info: YourProvateDataPayload) {
        NotificationCenter.default.post(name: Notification.Name.gotYourPrivateData, object: nil, userInfo: ["info": info])
    }
    
    func sendGotUsersPhotoNotification(info: UsersPhotoPayload) {
        NotificationCenter.default.post(name: Notification.Name.gotUsersPhoto, object: nil, userInfo: ["info": info])
    }
    
    func sendGotUsersInfoNotification(info: UsersInfoPayload) {
        NotificationCenter.default.post(name: Notification.Name.gotUsersInfo, object: nil, userInfo: ["info": info])
    }
}

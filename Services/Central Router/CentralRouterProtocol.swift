//
//  CentralRouterProtocol.swift
//  Farnsworth
//
//  Created by Marty on 12/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol CentralRouterProtocol {
    var chatDelegate: WSChatDelegate? { get set }

    func routePayload(_ payload: String, ofType type: IncomingMessageType)
}

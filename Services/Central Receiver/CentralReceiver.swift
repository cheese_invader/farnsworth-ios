//
//  CentralReceiver.swift
//  Farnsworth
//
//  Created by Marty on 12/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

class CentralReceiver {
    static let shared = CentralReceiver()
    private init() {}
    
    private let messagingQueue = DispatchQueue.init(label: "FS.Messaging.CentralReceiver")
}


extension CentralReceiver: CentralReceiverProtocol {
    
    func messageIsComing(_ message: String) {
        messagingQueue.async {
            guard let message = FarnsworthMessageDecoder().decode(message) else {
                return
            }
            
            CentralRouter.shared.routePayload(message.payload, ofType: message.messageType)
        }
    }
}

//
//  CentralReceiverProtocol.swift
//  Farnsworth
//
//  Created by Marty on 12/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

protocol CentralReceiverProtocol {
    func messageIsComing(_ message: String)
}

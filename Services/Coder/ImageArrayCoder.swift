//
//  ImageArrayCoder.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation
import UIKit

class ImageArrayCoder {
    let encoder = JSONEncoder()
    let decoder = JSONDecoder()
    
    func decodeImageArray(_ data: Data) -> [UIImage]? {
        guard let rawImages = try? decoder.decode([Data].self, from: data) else {
            return nil
        }
        var images = [UIImage]()
        
        for img in rawImages {
            guard let image = UIImage(data: img) else {
                return nil
            }
            images.append(image)
        }
        
        return images
    }
    
    func encodeImageArray(_ images: [UIImage]) -> Data? {
        var rawImages = [Data]()
        
        for image in images {
            guard let imgData = image.pngData() else {
                return nil
            }
            rawImages.append(imgData)
        }
        
        guard let data = try? encoder.encode(rawImages) else {
            return nil
        }
        
        return data
    }
}

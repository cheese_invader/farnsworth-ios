//
//  FarnsworthMessageDecoder.swift
//  Farnsworth
//
//  Created by Marty on 06/12/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

class FarnsworthMessageDecoder {
    private let decoder = JSONDecoder()
    
    init() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy HH:mm:ss zzz"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
    }
    
    func decode(_ data: Data) -> FSIncomingMessageSchema? {
        return try? decoder.decode(FSIncomingMessageSchema.self, from: data)
    }
    
    func decode(_ text: String) -> FSIncomingMessageSchema? {
        guard let data = text.data(using: .utf8) else {
            return nil
        }
        return decode(data)
    }
    
    func decodeNewChatIsComingPayload(_ text: String) -> NewChatIsComingPayload? {
        guard let data = text.data(using: .utf8) else {
            return nil
        }
        return try? decoder.decode(NewChatIsComingPayload.self, from: data)
    }
    
    func decodeCreateChatError(_ text: String) -> CreateChatErrorPayload? {
        guard let data = text.data(using: .utf8) else {
            return nil
        }
        return try? decoder.decode(CreateChatErrorPayload.self, from: data)
    }
    
    func decodeYourPrivateData(_ text: String) -> YourProvateDataPayload? {
        guard let data = text.data(using: .utf8) else {
            return nil
        }
        return try? decoder.decode(YourProvateDataPayload.self, from: data)
    }
    
    func decodeChatsDetailInfoPayload(_ text: String) -> ChatsDetailInfoPayload? {
        guard let data = text.data(using: .utf8) else {
            return nil
        }
        return try? decoder.decode(ChatsDetailInfoPayload.self, from: data)
    }
    
    func decodeFoundUsersPayload(_ text: String) -> FoundUsersPayload? {
        guard let data = text.data(using: .utf8) else {
            return nil
        }
        return try? decoder.decode(FoundUsersPayload.self, from: data)
    }
    
    func decodeUsersPhotoPayload(_ text: String) -> UsersPhotoPayload? {
        guard let data = text.data(using: .utf8) else {
            return nil
        }
        return try? decoder.decode(UsersPhotoPayload.self, from: data)
    }
    
    func decodeUsersInfoPayload(_ text: String) -> UsersInfoPayload? {
        guard let data = text.data(using: .utf8) else {
            return nil
        }
        return try? decoder.decode(UsersInfoPayload.self, from: data)
    }
}
